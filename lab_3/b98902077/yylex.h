#pragma once

#ifdef __cplusplus
extern "C" {
#endif

int yylex(void);

void yygraph_start(const char* filename);
void yygraph_end();
int yynode(const char* label, int nchildren, ...);

#ifdef __cplusplus
}
#endif

