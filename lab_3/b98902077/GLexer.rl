%%{
  machine kompiler_Lexer;
  
  alnum_u = alnum | '_';
  alpha_u = alpha | '_';
  
  long_comment = '/*' any* :>> '*/';
  
  string = '"' ( (^cntrl-[\\"]) | ('\\' [\\"]) )* :>> '"';
  
  word = alpha_u alnum_u*;
  
  dec = [0]|([1-9][0-9]*);
  hex = '0x' [0-9A-Fa-f]+;
  
  main := |*
    
    long_comment;
    space;
    
    #
    word { tokenCode = (GTOKEN_WORD); fbreak; printf("fuck"); };
    
    #
    string { tokenCode = (GTOKEN_STRING); fbreak; };
    
    #
    dec { tokenCode = (GTOKEN_INTEGER); fbreak; };
    
    #
    '+' { tokenCode = (GTOKEN_PLUS); fbreak; };
    '-' { tokenCode = (GTOKEN_MINUS); fbreak; };
    '*' { tokenCode = (GTOKEN_MULTIPLY); fbreak; };
    '/' { tokenCode = (GTOKEN_DIVIDE); fbreak; };
    '!' { tokenCode = (GTOKEN_LOGICAL_NOT); fbreak; };
    '&&' { tokenCode = (GTOKEN_LOGICAL_AND); fbreak; };
    '||' { tokenCode = (GTOKEN_LOGICAL_OR); fbreak; };
    '=' { tokenCode = (GTOKEN_ASSIGN); fbreak; };
    '==' { tokenCode = (GTOKEN_EQUAL); fbreak; };
    '!=' { tokenCode = (GTOKEN_NOT_EQUAL); fbreak; };
    '<' { tokenCode = (GTOKEN_LESS); fbreak; };
    '<=' { tokenCode = (GTOKEN_LESS_EQUAL); fbreak; };
    '>' { tokenCode = (GTOKEN_GREATER); fbreak; };
    '>=' { tokenCode = (GTOKEN_GREATER_EQUAL); fbreak; };
    '(' { tokenCode = (GTOKEN_ROUND_BRACKET_LEFT); fbreak; };
    ')' { tokenCode = (GTOKEN_ROUND_BRACKET_RIGHT); fbreak; };
    '[' { tokenCode = (GTOKEN_SQUARE_BRACKET_LEFT); fbreak; };
    ']' { tokenCode = (GTOKEN_SQUARE_BRACKET_RIGHT); fbreak; };
    '{' { tokenCode = (GTOKEN_CURLY_BRACKET_LEFT); fbreak; };
    '}' { tokenCode = (GTOKEN_CURLY_BRACKET_RIGHT); fbreak; };
    ',' { tokenCode = (GTOKEN_COMMA); fbreak; };
    ';' { tokenCode = (GTOKEN_SEMICOLON); fbreak; };
    
  *|;
  
}%%

#include "GLexer.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>

//
struct GToken
{
  int refcount;
  int code;
  char data[];
};

static GToken* _GTokenCreate(int code, const char* data, size_t datasize)
{
  GToken* token = (GToken*) malloc(sizeof(GToken)+datasize+1);
  std::memcpy(token->data, data, datasize);
  token->data[datasize] = '\0';
  token->code = code;
  token->refcount = 1;
  return token;
}

static void _GTokenSetCode(GTokenRef token, int code)
{
  GToken* tok = const_cast<GToken*>(token);
  tok->code = code;
}

int GTokenCode(GTokenRef token)
{
  return token->code;
}

const char* GTokenData(GTokenRef token)
{
  return token->data;
}

void GTokenRetain(GTokenRef token)
{
  assert( token->refcount > 0 );
  
  const_cast<GToken*>(token)->refcount++;
}

void GTokenRelease(GTokenRef token)
{
  assert( token->refcount > 0 );
  
  const_cast<GToken*>(token)->refcount--;
  
  if(token->refcount == 0)
    free(const_cast<void*>(static_cast<const void*>(token)));
}

int GTokenCompare(GTokenRef t1, GTokenRef t2)
{
  return strcmp(t1->data, t2->data);
}

//
namespace
{
  const size_t INIT_BUFSIZE = 512;
  
  %% write data;
}

//

void GLexer::initRagel()
{
  int& cs = m_RagelCs;
  int& act = m_RagelAct;
  char* & ts = m_RagelTs;
  char* & te = m_RagelTe;
  
  %% write init;
}

GTokenRef GLexer::execRagel()
{
  int& cs = m_RagelCs;
  int& act = m_RagelAct;
  char* & ts = m_RagelTs;
  char* & te = m_RagelTe;
  
  int tokenCode = GTOKEN_NONE;
  
  while(cs != %%{ write error; }%% && tokenCode == GTOKEN_NONE)
  {
    updateInput();
    
    char* & p = m_DataPtr;
    char* const & pe = m_DataPtrEnd;
    char* const & eof = m_DataEof;
    char* const & base = m_InputBuffer;
    
#ifdef GLEXER_TEST
    printf("start exec (p=%zd, pe=%zd, eof=%zd)\n", p-base, pe-base, eof-base);
#endif
    
    %% write exec;
    
#ifdef GLEXER_TEST
    printf("end exec (p=%zd, pe=%zd, eof=%zd)\n", p-base, pe-base, eof-base);
#endif
    
    if(p == eof)
      break;
  }
  
  if(cs == %%{ write error; }%%)
  {
    printf("lexical error\n");
  }
  else if(tokenCode > 0)
  {
    GToken* token = _GTokenCreate(tokenCode, ts, te-ts);
    
    // shrink input buffer
    shrinkInput(te);
    te = NULL, ts = NULL;
    
    return token;
  }
  
  return NULL;
}

//

GLexer::GLexer(std::istream& stream)
  : m_Stream(stream), 
    m_InputBuffer(NULL), m_InputBufferEnd(NULL), 
    m_DataPtr(NULL), m_DataPtrEnd(NULL), m_DataEof(NULL)
{
  initRagel();
}

GLexer::~GLexer()
{
  std::free(m_InputBuffer);
}

GTokenRef GLexer::getToken()
{
  return execRagel();
}

//

void GLexer::updateInput()
{
  if(m_DataPtr == m_DataPtrEnd)
  {
    // extend buffer space
    if(m_DataPtrEnd == m_InputBufferEnd)
    {
      std::size_t offset = m_DataPtr - m_InputBuffer;
      std::size_t dsiz = m_DataPtrEnd - m_DataPtr;
      std::size_t nsiz = (m_InputBufferEnd - m_InputBuffer);
      if(nsiz == 0)
        nsiz = INIT_BUFSIZE;
      else
        nsiz *= 2;
      
      char* newbuf = (char*) std::realloc(m_InputBuffer, nsiz);
      
      if(newbuf)
      {
        m_InputBuffer = newbuf;
        m_InputBufferEnd = newbuf + nsiz;
        
        m_DataPtr = newbuf + offset;
        m_DataPtrEnd = newbuf + dsiz + offset;
      }
      else
        std::abort();
    }
    
    // make room
    std::size_t room = m_InputBufferEnd - m_DataPtrEnd;
    char* base = m_DataPtrEnd;
    
    // read data
    //size_t result = fread(base, 1, room, stdin);
    m_Stream.read(base, room);
    
    std::size_t result = m_Stream.gcount();
    m_DataPtrEnd += result;
    
    if(m_Stream.eof())
      m_DataEof = m_DataPtrEnd;
  }
}

void GLexer::shrinkInput(char* te)
{
  char* base = m_InputBuffer;
  char* p = m_DataPtr;
  char* pe = m_DataPtrEnd;
  char* eof = m_DataEof;
  
  if(te - p < 0)
    return;
  
  std::memmove(base, te, pe - te);
  
  m_DataPtr = base;
  m_DataPtrEnd = base + (pe - te);
  if(eof)
    m_DataEof = m_DataPtrEnd;
}

