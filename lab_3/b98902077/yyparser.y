%{
  #include "yylex.h"
  #include <stdio.h>
  #include <stdlib.h>
  #include <stdarg.h>

  void yyerror(const char* );
  
%}

%union {
  int node;
}

%token <node> ERROR

%token <node> ID STR NUM

%token <node> PLUS MINUS MULTIPLY DIVIDE
%token <node> LOGICAL_NOT LOGICAL_AND LOGICAL_OR
%token <node> ASSIGN
%token <node> EQUAL NEQUAL LESS LEQUAL GREATER GEQUAL
%token <node> PARENTHESE_OPEN PARENTHESE_CLOSE
%token <node> BRACKET_OPEN BRACKET_CLOSE
%token <node> BRACE_OPEN BRACE_CLOSE
%token <node> COMMA SEMICOLON

%token <node> INT IF ELSE WHILE BREAK CONTINUE
%token <node> SCAN PRINT PRINTLN

%start program

%type <node> program blockStmt var dims null
%type <node> varDecls varDecl idList
%type <node> stmts stmt
%type <node> arithExpr arithAddSub arithMulDiv arithUnary
%type <node> varList printableList printable
%type <node> logicExpr logicOr logicAnd logicNot logicBase

%%

program: blockStmt { $$ = yynode("program", 1, $1); }
       ;

null: { $$ = yynode("<null>", 0); }
    ;

blockStmt: BRACE_OPEN varDecls stmts BRACE_CLOSE { 
             $$ = yynode("blockStmt", 4, $1, $2, $3, $4); 
           }
         ;

varDecls: varDecls varDecl { $$ = yynode("varDecls", 2, $1, $2); }
        | null { $$ = yynode("varDecls", 1, $1); }
        ;

varDecl: INT dims idList SEMICOLON { 
           $$ = yynode("varDecl", 4, $1, $2, $3, $4); 
         }
       | INT idList SEMICOLON { 
           $$ = yynode("varDecl", 3, $1, $2, $3); 
         }
       ;

idList: idList COMMA ID { $$ = yynode("idList", 3, $1, $2, $3); }
      | ID { $$ = yynode("idList", 1, $1); }
      ;

dims: dims BRACKET_OPEN arithExpr BRACKET_CLOSE {
        $$ = yynode("dimDecl", 4, $1, $2, $3, $4);
      }
    | BRACKET_OPEN arithExpr BRACKET_CLOSE {
        $$ = yynode("dimDecl", 3, $1, $2, $3);
      }
    ;

stmts: stmts stmt { $$ = yynode("stmts", 2, $1, $2); }
     | null { $$ = yynode("stmts", 1, $1); }
     ;

stmt: var ASSIGN arithExpr SEMICOLON { $$ = yynode("stmt", 4, $1,$2,$3,$4); }
    | IF PARENTHESE_OPEN logicExpr PARENTHESE_CLOSE blockStmt {
        $$ = yynode("stmt", 5, $1, $2, $3, $4, $5);
      }
    | IF PARENTHESE_OPEN logicExpr PARENTHESE_CLOSE blockStmt ELSE blockStmt {
        $$ = yynode("stmt", 7, $1, $2, $3, $4, $5, $6, $7);
      }
    | WHILE PARENTHESE_OPEN logicExpr PARENTHESE_CLOSE blockStmt {
        $$ = yynode("stmt", 5, $1, $2, $3, $4, $5);
      }
    | BREAK SEMICOLON { $$ = yynode("stmt", 2, $1, $2); }
    | CONTINUE SEMICOLON { $$ = yynode("stmt", 2, $1, $2); }
    | SCAN PARENTHESE_OPEN varList PARENTHESE_CLOSE SEMICOLON {
        $$ = yynode("stmt", 5, $1, $2, $3, $4, $5);
      }
    | PRINT PARENTHESE_OPEN printableList PARENTHESE_CLOSE SEMICOLON {
        $$ = yynode("stmt", 5, $1, $2, $3, $4, $5);
      }
    | PRINTLN PARENTHESE_OPEN printableList PARENTHESE_CLOSE SEMICOLON {
        $$ = yynode("stmt", 5, $1, $2, $3, $4, $5);
      }
    | blockStmt { $$ = yynode("stmt", 1, $1); }
    ;

varList: varList COMMA var { $$ = yynode("varList", 3, $1, $2, $3); }
       | var { $$ = yynode("varList", 1, $1); }
       ;

printableList: printableList COMMA printable { 
                 $$ = yynode("printableList", 3, $1, $2, $3);
               }
             | printable { $$ = yynode("printableList", 1, $1); }
             ;

printable: STR { $$ = yynode("printable", 1, $1); }
         | arithExpr { $$ = yynode("printable", 1, $1); }
         ;

var: ID { $$ = yynode("var", 1, $1); }
   | ID dims { $$ = yynode("var", 2, $1, $2); }
   ;

arithExpr: arithAddSub { $$ = yynode("arithmetic", 1, $1); }
         ;

arithAddSub: arithAddSub PLUS arithMulDiv {
               $$ = yynode("add_sub", 3, $1, $2, $3); 
             }
           | arithAddSub MINUS arithMulDiv {
               $$ = yynode("add_sub", 3, $1, $2, $3); 
             }
           | arithMulDiv { $$ = yynode("add_sub", 1, $1); }
           ;

arithMulDiv: arithMulDiv MULTIPLY arithUnary {
               $$ = yynode("mul_div", 3, $1, $2, $3); 
             }
           | arithMulDiv DIVIDE arithUnary { 
               $$ = yynode("mul_div", 3, $1, $2, $3); 
             }
           | arithUnary { $$ = yynode("mul_div", 1, $1); }
           ;

arithUnary: PLUS arithUnary { $$ = yynode("unary", 2, $1, $2); }
          | MINUS arithUnary { $$ = yynode("unary", 2, $1, $2); }
          | var { $$ = yynode("unary", 1, $1); }
          | NUM { $$ = yynode("unary", 1, $1); }
          | PARENTHESE_OPEN arithExpr PARENTHESE_CLOSE { 
             $$ = yynode("unary", 3, $1, $2, $3); 
            }
          ;

logicExpr: logicOr { $$ = yynode("logical", 1, $1); }
         ;

logicOr: logicOr LOGICAL_OR logicAnd { $$ = yynode("or", 3, $1, $2, $3); }
       | logicAnd { $$ = yynode("or", 1, $1); }
       ;

logicAnd: logicAnd LOGICAL_AND logicNot { $$ = yynode("and", 3, $1, $2, $3); }
        | logicNot { $$ = yynode("and", 1, $1); }
        ;

logicNot: LOGICAL_NOT logicNot { $$ = yynode("not", 2, $1, $2); }
        | logicBase { $$ = yynode("not", 1, $1); }
        ;

logicBase: arithExpr EQUAL arithExpr { $$ = yynode("lobase", 3, $1, $2, $3); }
         | arithExpr NEQUAL arithExpr { $$ = yynode("lobase", 3, $1, $2, $3); }
         | arithExpr LESS arithExpr { $$ = yynode("lobase", 3, $1, $2, $3); }
         | arithExpr LEQUAL arithExpr { $$ = yynode("lobase", 3, $1, $2, $3); }
         | arithExpr GREATER arithExpr { $$ = yynode("lobase", 3, $1, $2, $3); }
         | arithExpr GEQUAL arithExpr { $$ = yynode("lobase", 3, $1, $2, $3); }
         | BRACKET_OPEN logicExpr BRACKET_CLOSE {
             $$ = yynode("lobase", 3, $1, $2, $3);
           }
         ;

%%

int main(int argc, char* argv[])
{
  int result, dotgraph = 0;
  
  if(argc == 2)
    yygraph_start(argv[1]), dotgraph = 1;
  
  result = yyparse();
  
  if(result == 0)
    printf("Pass\n");
  else
    printf("Fail\n");
  
  if(dotgraph)
    yygraph_end();
  
  return 0;
}

void yyerror(const char* msg)
{
  //fprintf(stderr, "parser error: %s\n", msg);
}

