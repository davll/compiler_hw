#pragma once

#include <stddef.h>
#include <istream>

//
//
enum GTokenCode
{
  GTOKEN_NONE = 0,
  
  // Symbol
  GTOKEN_WORD,
  
  // String Literal
  GTOKEN_STRING,
  
  // Integer
  GTOKEN_INTEGER,
  
  // Operators
  
  GTOKEN_PLUS, // +
  GTOKEN_MINUS, // -
  GTOKEN_MULTIPLY, // *
  GTOKEN_DIVIDE, // /
  
  GTOKEN_LOGICAL_NOT, // !
  GTOKEN_LOGICAL_AND, // &&
  GTOKEN_LOGICAL_OR, // ||
  
  GTOKEN_ASSIGN, // =
  
  GTOKEN_EQUAL, // ==
  GTOKEN_NOT_EQUAL, // !=
  GTOKEN_LESS, // <
  GTOKEN_LESS_EQUAL, // <=
  GTOKEN_GREATER, // >
  GTOKEN_GREATER_EQUAL, // >=
  
  GTOKEN_ROUND_BRACKET_LEFT, // (
  GTOKEN_ROUND_BRACKET_RIGHT, // )
  GTOKEN_SQUARE_BRACKET_LEFT, // [
  GTOKEN_SQUARE_BRACKET_RIGHT, // ]
  GTOKEN_CURLY_BRACKET_LEFT, // {
  GTOKEN_CURLY_BRACKET_RIGHT, // }
  
  GTOKEN_COMMA, // ,
  GTOKEN_SEMICOLON, // ;
  
  GTOKEN_COUNT_
};

//
//
//
typedef const struct GToken* GTokenRef;

int GTokenCode(GTokenRef token);
const char* GTokenData(GTokenRef token);
void GTokenRetain(GTokenRef token);
void GTokenRelease(GTokenRef token);
int GTokenCompare(GTokenRef t1, GTokenRef t2);

//
//
//
class GLexer
{
private:
  void initRagel();
  GTokenRef execRagel();
  
public:
  GLexer(std::istream& stream);
  ~GLexer();
  
  GTokenRef getToken();
  
private:
  void updateInput();
  void shrinkInput(char* te);
  
private:
  std::istream& m_Stream;
  
  char* m_InputBuffer;
  char* m_InputBufferEnd;
  
  char* m_DataPtr;
  char* m_DataPtrEnd;
  char* m_DataEof;
  
  int m_RagelCs;
  int m_RagelAct;
  char* m_RagelTs;
  char* m_RagelTe;
};
