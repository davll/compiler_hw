#include "GLexer.h"
#include <iostream>
#include <stdio.h>

int main()
{
  GTokenRef tok;
  GLexer lex(std::cin);
  
  while((tok = lex.getToken()) != NULL)
  {
    printf("code = %d, data = %s\n", GTokenCode(tok), GTokenData(tok));
    GTokenRelease(tok);
  }
  
  for(int i = 0; i < 5; ++i)
    printf("%p\n", lex.getToken());
  
  return 0;
}

