#include "yylex.h"

#include "GLexer.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>

#include "yyparser.h"

static GLexer lexer(std::cin);

extern "C"
int yylex(void)
{
  GTokenRef tok = lexer.getToken();
  
  if(tok == NULL)
  {
    yylval.node = -1;
    return 0;
  }
  
  const char* data = GTokenData(tok);
  int code = GTokenCode(tok);
  
  if(code == GTOKEN_STRING)
  {
    char* tmp = (char*)malloc(strlen(data)+3);
    tmp[0] = '\\';
    strcpy(tmp+1, data);
    tmp[strlen(data)] = '\\';
    tmp[strlen(data)+1] = '\"';
    tmp[strlen(data)+2] = '\0';
    yylval.node = yynode(tmp, 0);
    free(tmp);
  }
  else
    yylval.node = yynode(data, 0);
  
  if(code == GTOKEN_WORD)
  {
    if(strcmp(data, "int") == 0)
      return INT;
    else if(strcmp(data, "if") == 0)
      return IF;
    else if(strcmp(data, "else") == 0)
      return ELSE;
    else if(strcmp(data, "while") == 0)
      return WHILE;
    else if(strcmp(data, "break") == 0)
      return BREAK;
    else if(strcmp(data, "continue") == 0)
      return CONTINUE;
    else if(strcmp(data, "scan") == 0)
      return SCAN;
    else if(strcmp(data, "print") == 0)
      return PRINT;
    else if(strcmp(data, "println") == 0)
      return PRINTLN;
  }
  
  switch(code)
  {
    case GTOKEN_WORD: return ID;
    case GTOKEN_STRING: return STR;
    case GTOKEN_INTEGER: return NUM;
    case GTOKEN_PLUS: return PLUS;
    case GTOKEN_MINUS: return MINUS;
    case GTOKEN_MULTIPLY: return MULTIPLY;
    case GTOKEN_DIVIDE: return DIVIDE;
    case GTOKEN_LOGICAL_NOT: return LOGICAL_NOT;
    case GTOKEN_LOGICAL_AND: return LOGICAL_AND;
    case GTOKEN_LOGICAL_OR: return LOGICAL_OR;
    case GTOKEN_ASSIGN: return ASSIGN;
    case GTOKEN_EQUAL: return EQUAL;
    case GTOKEN_NOT_EQUAL: return NEQUAL;
    case GTOKEN_LESS: return LESS;
    case GTOKEN_LESS_EQUAL: return LEQUAL;
    case GTOKEN_GREATER: return GREATER;
    case GTOKEN_GREATER_EQUAL: return GEQUAL;
    case GTOKEN_ROUND_BRACKET_LEFT: return PARENTHESE_OPEN;
    case GTOKEN_ROUND_BRACKET_RIGHT: return PARENTHESE_CLOSE;
    case GTOKEN_SQUARE_BRACKET_LEFT: return BRACKET_OPEN;
    case GTOKEN_SQUARE_BRACKET_RIGHT: return BRACKET_CLOSE;
    case GTOKEN_CURLY_BRACKET_LEFT: return BRACE_OPEN;
    case GTOKEN_CURLY_BRACKET_RIGHT: return BRACE_CLOSE;
    case GTOKEN_COMMA: return COMMA;
    case GTOKEN_SEMICOLON: return SEMICOLON;
  }
  
  return ERROR;
}

namespace
{
  FILE* fpDot = NULL;
}

extern "C"
void yygraph_start(const char* filename)
{
  fpDot = fopen(filename, "w");
  
  if(fpDot)
  {
    fprintf(fpDot, "digraph G {\n");
    fprintf(fpDot, "  graph [ordering=\"out\"];\n");
    //fprintf(fpDot, "  node [shape=box];\n");
  }
}

extern "C"
void yygraph_end()
{
  if(fpDot)
  {
    fprintf(fpDot, "};\n");
    fclose(fpDot);
  }
}

extern "C"
int yynode(const char* label, int nchildren, ...)
{
  static int nodeCount = 0;
  int n = ++nodeCount;
  va_list args;
  
  if(fpDot != NULL)
  {
    if(nchildren == 0)
      fprintf(fpDot, "  node_%d [label=\"%s\", shape=oval];\n", n, label);
    else
      fprintf(fpDot, "  node_%d [label=\"%s\", shape=box];\n", n, label);
  }
  
  va_start(args, nchildren);
  
  for(int i = 0; i < nchildren; ++i)
  {
    int c = va_arg(args, int);
    
    if(fpDot != NULL)
      fprintf(fpDot, "  node_%d -> node_%d;\n", n, c);
  }
  
  va_end(args);
  
  return n;
}
