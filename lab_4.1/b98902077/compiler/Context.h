#pragma once

#include "TokenExtractor.h"
#include "lib/StringMap.h"

union YYSTYPE;

namespace cmm
{
  
  class Context
  {
  public:
    Context();
    ~Context();
    
    bool parse();
    
  public: // for YYParser.y
    int _lex(YYSTYPE* lvalp);
    
  private:
    int _lex_word(YYSTYPE* lvalp);
    int _lex_string(YYSTYPE* lvalp);
    int _lex_decimal(YYSTYPE* lvalp);
    
  private: // 
    Context(const Context&);
    void operator=(const Context&);
    
  private:
    TokenExtractor m_TokenExtractor;
    
  };
  
}
