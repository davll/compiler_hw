#pragma once

#include "StringRef.h"

#include <string>
#include <map>
//#include <unordered_map>

namespace cmm
{
  
  /// @todo: remove std::map in the future
  /// @class: Dictionary with key=StringRef
  template <typename ValueTy>
  class StringMap
  {
  public:
    typedef StringRef key_type;
    typedef ValueTy mapped_type;
    typedef std::size_t size_type;
    
  public:
    StringMap()
    {
    }
    
    ~StringMap()
    {
    }
    
    size_type count(const StringRef& key) const
    {
      auto i = m_Map.find(key.str());
      return ( i == m_Map.end() ? 0 : 1 );
    }
    
    ValueTy& operator[](const StringRef& key)
    {
      return m_Map[key.str()];
    }
    
    const ValueTy& operator[](const StringRef& key) const
    {
      return m_Map[key.str()];
    }
    
  private:
    std::map<std::string, ValueTy /* Compare, Allocator */> m_Map;
    
  };
  
}
