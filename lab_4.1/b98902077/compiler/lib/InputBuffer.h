#pragma once

#include <cstddef>
#include <cassert>
#include <cstring>
#include <cstdlib>
#include <istream>

namespace cmm
{
  
  /// @class Input Buffer
  class InputBuffer
  {
  public:
    typedef const char* iterator;
    typedef const char* const_iterator;
    typedef std::size_t size_type;
    
  public:
    
    /// @name Constructor
    explicit InputBuffer(std::istream& input);
    
    /// @name Destructor
    ~InputBuffer();
    
    /// @name Access
    /// @{
    iterator begin() const { return m_Reserved; }
    iterator end() const { return m_Data + m_Length; }
    
    size_type size() const { return end() - begin(); }
    
    bool eof() const { return m_Eof; }
    
    /// @}
    
    /// @name Update Data
    /// @{
    
    void update();
    void flush(size_type nbytes);
    
    /// @}
    
  private:
    /// @note Uncopyable
    
    InputBuffer(const InputBuffer& ); // = delete;
    void operator=(const InputBuffer& ); // = delete;
    
  private:
    std::size_t m_Length;
    char*       m_Data;
    char*       m_Reserved;
    bool        m_Eof;
    
    std::istream& m_Stream;
    
    // 
    // m_Data      m_Reserved
    // |           |
    // V           V
    // xxxxxxxxxxxxooooooooooooooooooooooooooooooooooo
    // |<---------------- m_Length ----------------->|
    //
    // x denotes data to be flushed
    // o denotes data that will be used later
    // 
    
    enum { GROWTH_DELTA = 1024 };
    
  };
  
  //
  inline InputBuffer::InputBuffer(std::istream& input)
  : m_Length(0), m_Data(NULL), m_Reserved(NULL), m_Eof(false), m_Stream(input)
  {
  }
  
  //
  inline InputBuffer::~InputBuffer()
  {
    std::free(m_Data);
  }
  
  //
  inline void InputBuffer::update()
  {
    if (m_Eof) return;
    
    // Fetch Attributes
    char * pData = m_Data, * pResv = m_Reserved;
    std::size_t zLen = m_Length;
    
    // Compute Number of Bytes to be flushed
    std::ptrdiff_t bytesFlushed = pResv - pData;
    
    // Declare Room Size
    std::size_t zRoom;
    
    //
    if (bytesFlushed > 0)
    {
      // Move Data Buffer
      std::memmove(pData, pResv, zLen - bytesFlushed);
      
      // Move m_Reserved Position
      pResv = pData;
      
      // Set Room
      zRoom = bytesFlushed;
    }
    else
    {
      // Expand Size
      std::size_t newLen = zLen + GROWTH_DELTA;
      
      // Re-allocate Data Buffer
      char* ptr = (char*) std::realloc(pData, newLen);
      assert((ptr != NULL) && "data re-allocation failed");
      
      // Set Room
      zRoom = newLen - zLen;
      
      // Update
      pResv = pData = ptr, zLen = newLen;
    }
    
    // Read Block from File
    {
      m_Stream.read(pData + zLen - zRoom, zRoom);
      
      std::size_t zRead = m_Stream.gcount(); // bytes read
      
      if (m_Stream.eof()) // eof detected
      {
        zLen = zLen - zRoom + zRead;
        m_Eof = true;
      }
      else if (m_Stream.bad()) // error detected
      {
        std::abort();
      }
    }
    
    // Update Attributes
    m_Data = pData;
    m_Reserved = pResv;
    m_Length = zLen;
  }
  
  inline void InputBuffer::flush(size_type nbytes)
  {
    assert(((m_Reserved - m_Data + nbytes) <= m_Length) && "");
    
    m_Reserved += nbytes;
  }
  
}
