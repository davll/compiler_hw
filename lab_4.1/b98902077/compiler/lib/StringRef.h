#pragma once

#include <cstddef>
#include <cstdlib>
#include <cstring>
#include <cassert>

#include <algorithm>
#include <string>
#include <utility>
#include <limits>
#include <ostream>

namespace cmm
{
  
  /// @class String Reference
  class StringRef
  {
  public:
    typedef const char* iterator;
    typedef const char* const_iterator;
    typedef std::size_t size_type;
    
    //static const std::size_t npos = static_cast<std::size_t>(-1);
    enum : std::size_t {
      npos = static_cast<std::size_t>(-1)
    };
    
  public:
    
    /// @name Constructors
    /// @{
    
    // Construct an empty string ref
    StringRef() : m_Data(NULL), m_Length(0) {}
    
    // Construct a string ref from a cstring
    StringRef(const char* str) : m_Data(str)
    {
      assert((str != NULL) && "StringRef cannot be build from a NULL arg");
      m_Length = std::strlen(str);
    }
    
    // Construct a string ref from a pointer and length
    StringRef(const char* data, size_t len) : m_Data(data), m_Length(len)
    {
      assert((data != NULL || len == 0) &&
             "StringRef cannot be build from a NULL argument"
             " with non-zero length");
    }
    
    // Construct a string ref from a std::string
    StringRef(const std::string& str)
    : m_Data(str.data()), m_Length(str.length())
    {
    }
    
    // Copy-Construct a string ref
    StringRef(const StringRef& o) : m_Data(o.m_Data), m_Length(o.m_Length) {}
    
    /// @}
    
    /// @name Iterators
    /// @{
    
    iterator begin() const { return m_Data; }
    iterator end() const { return m_Data + m_Length; }
    
    /// @}
    
    /// @name String Operations
    /// @{
    
    const char* data() const { return m_Data; }
    bool empty() const { return m_Length == 0; }
    size_type size() const { return m_Length; }
    size_type length() const { return m_Length; }
    
    std::string str() const
    {
      if (m_Length == 0) return std::string();
      return std::string(m_Data, m_Length);
    }
    
    /// @}
    
    /// @name String Comparison
    /// @{
    
    bool equals(const StringRef& rhs) const
    {
      return (m_Length == rhs.m_Length &&
              my_memcmp(m_Data, rhs.m_Data, m_Length) == 0);
    }
    
    int compare(const StringRef& rhs) const
    {
      size_type len = m_Length, rlen = rhs.m_Length;
      
      if(int ret = my_memcmp(m_Data, rhs.m_Data, my_min(len, rlen)) != 0)
        return (ret < 0 ? -1 : 1);
      
      if(len == rlen) return 0;
      return len < rlen ? -1 : 1;
    }
    
    int compare_nocase(const StringRef& rhs) const;
    int compare_numeric(const StringRef& rhs)const;
    
    // edit_distance
    
    /// @}
    
    /// @name String Searching
    /// @{
    
    size_type find(char ch, size_type from = 0) const;
    size_type rfind(char ch, size_type from = npos) const;
    size_type find(StringRef str, size_type from = 0) const;
    size_type rfind(StringRef str, size_type from = npos) const;
    
    // find_first_of
    // find_first_not_of
    // find_last_of
    
    // count
    
    /// @}
    
    /// @name Substring Operations
    /// @{
    
    StringRef substr(size_type start, size_type n = npos) const
    {
      const size_type len = m_Length;
      size_type s = my_min(start, len);
      return StringRef(m_Data + s, my_min(n, len - s));
    }
    
    StringRef slice(size_type start, size_type end) const;
    std::pair<StringRef, StringRef> split(char separator) const;
    std::pair<StringRef, StringRef> split(StringRef separator) const;
    std::pair<StringRef, StringRef> rsplit(char separator) const;
    std::pair<StringRef, StringRef> rsplit(StringRef separator) const;
    
    /// @}
    
  private:
    const char* m_Data;
    std::size_t m_Length;
    
    static std::size_t my_min(std::size_t a, std::size_t b)
    {
      return (a < b ? a : b);
    }
    
    static std::size_t my_max(std::size_t a, std::size_t b)
    {
      return (a > b ? a : b);
    }
    
    static int my_memcmp(const char* lhs, const char* rhs, std::size_t len)
    {
      if (len == 0) return 0;
      return std::memcmp(lhs, rhs, len);
    }
    
  };
  
  /// @name StringRef Comparison Operators
  /// @{
  
  inline bool operator==(const StringRef& lhs, const StringRef& rhs)
  {
    return lhs.equals(rhs);
  }
  
  inline bool operator!=(const StringRef& lhs, const StringRef& rhs)
  {
    return !(lhs == rhs);
  }
  
  inline bool operator<(const StringRef& lhs, const StringRef& rhs)
  {
    return lhs.compare(rhs) < 0;
  }
  
  inline bool operator<=(const StringRef& lhs, const StringRef& rhs)
  {
    return lhs.compare(rhs) <= 0;
  }
  
  inline bool operator>(const StringRef& lhs, const StringRef& rhs)
  {
    return lhs.compare(rhs) > 0;
  }
  
  inline bool operator>=(const StringRef& lhs, const StringRef& rhs)
  {
    return lhs.compare(rhs) >= 0;
  }
  
  /// @}
  
  inline std::ostream& operator<<(std::ostream& ost, const StringRef& str)
  {
    for (StringRef::iterator i = str.begin(), e = str.end(); i != e; ++i)
      ost << *i;
    return ost;
  }
  
}
