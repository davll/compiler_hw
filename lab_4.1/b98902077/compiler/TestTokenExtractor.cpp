#include "TokenExtractor.h"
#include <iostream>

using namespace cmm;

int main()
{
  TokenExtractor extractor("/dev/stdin");
  
  while(extractor.next())
  {
    std::cout << "Token: code = " << (int)extractor.tokenCode();
    std::cout << ", data = \"" << extractor.tokenData() << "\"\n";
  }
  
  return 0;
}
