%%{
  machine cmm_TokenExtractor;
  
  alnum_u = alnum | '_';
  alpha_u = alpha | '_';
  
  long_comment = '/*' any* :>> '*/';
  
  string = '"' ( (^cntrl-[\\"]) | ('\\' [\\"]) )* :>> '"';
  
  word = alpha_u alnum_u*;
  
  dec = [0]|([1-9][0-9]*);
  hex = '0x' [0-9A-Fa-f]+;
  
  main := |*
    
    long_comment;
    space;
    
    #
    word { tc = TokenCode::Word; fbreak; };
    
    #
    string { tc = TokenCode::String; fbreak; };
    
    #
    dec { tc = TokenCode::Decimal; fbreak; };
    
    #
    '+' { tc = TokenCode::Plus; fbreak; };
    '-' { tc = TokenCode::Minus; fbreak; };
    '*' { tc = TokenCode::Star; fbreak; };
    '/' { tc = TokenCode::Slash; fbreak; };
    
    #
    '!' { tc = TokenCode::Exclamation; fbreak; };
    '&&' { tc = TokenCode::Ampersand2; fbreak; };
    '||' { tc = TokenCode::Pipe2; fbreak; };
    
    #
    '=' { tc = TokenCode::Equal; fbreak; };
    
    #
    '==' { tc = TokenCode::Equal2; fbreak; };
    '!=' { tc = TokenCode::Exclamation_Equal; fbreak; };
    '<' { tc = TokenCode::LeftAngledBracket; fbreak; };
    '<=' { tc = TokenCode::LeftAngledBracket_Equal; fbreak; };
    '>' { tc = TokenCode::RightAngledBracket; fbreak; };
    '>=' { tc = TokenCode::RightAngledBracket_Equal; fbreak; };
    
    #
    '(' { tc = TokenCode::LeftParenthesis; fbreak; };
    ')' { tc = TokenCode::RightParenthesis; fbreak; };
    '[' { tc = TokenCode::LeftBracket; fbreak; };
    ']' { tc = TokenCode::RightBracket; fbreak; };
    '{' { tc = TokenCode::LeftBrace; fbreak; };
    '}' { tc = TokenCode::RightBrace; fbreak; };
    
    #
    ',' { tc = TokenCode::Comma; fbreak; };
    ';' { tc = TokenCode::Semicolon; fbreak; };
    
  *|;
  
}%%

#include "TokenExtractor.h"
#include <cassert>

namespace cmm
{
  namespace
  {
    %% write data;
  }
  
  void TokenExtractor::fsmInit()
  {
    m_FsmState = %%{ write start; }%% ;
    
    m_TokenStart = NULL;
    m_TokenEnd = NULL;
    m_TokenCode = TokenCode::None;
    
    while (!m_Buffer.eof())
      m_Buffer.update();
    m_FsmCurr = m_Buffer.begin();
  }
  
  bool TokenExtractor::fsmNextToken()
  {
    int cs = m_FsmState;
    const char *ts = NULL, *te = NULL;
    
    const char *p = m_FsmCurr, *pe = m_Buffer.end();
    const char *eof = pe;
    TokenCode tc = TokenCode::None;
    
    %% write exec;
    
    m_FsmState = cs;
    m_FsmCurr = te;
    m_TokenStart = ts;
    m_TokenEnd = te;
    m_TokenCode = tc;
    
    if (fsmError() || tc == TokenCode::Error)
    {
      m_TokenStart = NULL;
      m_TokenEnd = NULL;
      m_TokenCode = TokenCode::Error;
      return false;
    }
    
    return tc != TokenCode::None;
  }
  
  bool TokenExtractor::fsmError()
  {
    return (m_FsmState == %%{ write error; }%%);
  }
  
  // ===============================================================================
  
  TokenExtractor::TokenExtractor(const char* filename)
  : m_File(filename, std::ifstream::in | std::ifstream::binary),
  m_Buffer(m_File),
  m_TokenStart(NULL), m_TokenEnd(NULL)
  {
    assert(m_File.is_open() && "Input File cannot be open");
    
    fsmInit();
  }
  
  TokenExtractor::~TokenExtractor()
  {
  }
  
  bool TokenExtractor::next()
  {
    if (fsmError())
      return false;
    
    return fsmNextToken();
  }
  
  StringRef TokenExtractor::tokenData() const
  {
    if (m_TokenCode == TokenCode::None)
      return StringRef(NULL, 0);
    
    return StringRef(m_TokenStart, m_TokenEnd - m_TokenStart);
  }
  
  TokenCode TokenExtractor::tokenCode() const
  {
    return m_TokenCode;
  }
  
}
