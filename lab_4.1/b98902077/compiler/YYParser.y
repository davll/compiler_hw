%{

#include "Context.h"
#include <iostream>

union YYSTYPE;

static int yylex(YYSTYPE *lvalp, cmm::Context& context);

static int yyparse(cmm::Context& context);

static void yyerror(cmm::Context& context, const char* msg);

%}

%require "2.3"

%output="YYParser.cpp"
%defines

//%locations
%pure-parser
/*%define api.pure*/

// yylex args
%lex-param   { cmm::Context& context }

// yyparse args
%parse-param { cmm::Context& context }

// Terminal + Non-Terminal Data Types
%union {
  void* null;
}

// Tokens
%token <null> YX_ERROR
%token <null> YX_ID // TODO
%token <null> YX_STR // TODO
%token <null> YX_NUM // TODO
%token <null> YX_INT                 "int"
%token <null> YX_IF                  "if"
%token <null> YX_ELSE                "else"
%token <null> YX_WHILE               "while"
%token <null> YX_BREAK               "break"
%token <null> YX_CONTINUE            "continue"
%token <null> YX_SCAN                "scan"
%token <null> YX_PRINT               "print"
%token <null> YX_PRINTLN             "println"

// Operator Tokens
%token <null> YX_PLUS                 "+"
%token <null> YX_MINUS                "-"
%token <null> YX_MULTIPLY             "*"
%token <null> YX_DIVIDE               "/"
%token <null> YX_LOGICAL_NOT          "!"
%token <null> YX_LOGICAL_AND          "&&"
%token <null> YX_LOGICAL_OR           "||"
%token <null> YX_ASSIGN               "="
%token <null> YX_EQUAL                "=="
%token <null> YX_NEQUAL               "!="
%token <null> YX_LESS                 "<"
%token <null> YX_LEQUAL               "<="
%token <null> YX_GREATER              ">"
%token <null> YX_GEQUAL               ">="
%token <null> YX_PARENTHESE_OPEN      "("
%token <null> YX_PARENTHESE_CLOSE     ")"
%token <null> YX_BRACKET_OPEN         "["
%token <null> YX_BRACKET_CLOSE        "]"
%token <null> YX_BRACE_OPEN           "{"
%token <null> YX_BRACE_CLOSE          "}"
%token <null> YX_COMMA                ","
%token <null> YX_SEMICOLON            ";"

// Operator Precedence
%left "||"
%left "&&"
%left "!"
%nonassoc "==" "!=" "<" "<=" ">" ">="
%left "+" "-"
%left "*" "/"
%right YX_UNARY

// Define Start Symbol
%start program

%%

// Start Symbol
program: blockStmt
       ;

// Block Sematics
blockStmt: "{" varDeclSeq stmtSeq "}"
         ;

// Variable Declaration Sequence
varDeclSeq: varDeclSeq varDecl
          | /* NULL */
          ;

// Variable Declaration
varDecl: "int" dims idList ";"
      | "int" idList ";"
      ;

// Identifier Sequence
idList: idList "," YX_ID
      | YX_ID
      ;

// Dimension Sequence
dims: dims "[" arithExpr "]"
    | "[" arithExpr "]"
    ;

// Sematics Sequence
stmtSeq: stmtSeq stmt
     | /* NULL */
     ;

// Sematics
stmt: var "=" arithExpr ";";
stmt: "if" "(" logicExpr ")" blockStmt;
stmt: "if" "(" logicExpr ")" blockStmt "else" blockStmt;
stmt: "while" "(" logicExpr ")" blockStmt;
stmt: "break" ";";
stmt: "continue" ";";
stmt: "scan" "(" varList ")" ";";
stmt: "print" "(" printableList ")" ";";
stmt: "println" "(" printableList ")" ";";
stmt: blockStmt;

// Variable Sequence
varList: varList "," var
       | var
       ;

// Printable Object Sequence
printableList: printableList "," printable
             | printable
             ;

// Printable Object
printable: YX_STR
         | arithExpr
         ;

// Variable Access
var: YX_ID
   | YX_ID dims
   ;

// Arithmetic Expression
arithExpr: arithExpr "+" arithExpr
         | arithExpr "-" arithExpr
         | arithExpr "*" arithExpr
         | arithExpr "/" arithExpr
         | "+" arithExpr %prec YX_UNARY
         | "-" arithExpr %prec YX_UNARY
         | var
         | YX_NUM
         | "(" arithExpr ")"
         ;

// Logical Expression
logicExpr: logicExpr "||" logicExpr
         | logicExpr "&&" logicExpr
         | "!" logicExpr
         | arithExpr "==" arithExpr
         | arithExpr "!=" arithExpr
         | arithExpr "<" arithExpr
         | arithExpr "<=" arithExpr
         | arithExpr ">" arithExpr
         | arithExpr ">=" arithExpr
         | "[" logicExpr "]"
         ;

%%

//
//
//
static int yylex(YYSTYPE *lvalp, cmm::Context& context)
{
  int result = context._lex(lvalp);
  
  if (result == YX_ERROR)
    std::cerr << "Lexical Error" << std::endl;
  
  return result;
}

//
//
//
static void yyerror(cmm::Context& context, const char* msg)
{
  std::cerr << msg << std::endl;
}

//
//
//
namespace cmm
{
  
  bool Context::parse()
  {
    return yyparse(*this) == 0;
  }
  
}

