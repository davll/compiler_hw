#pragma once

#include "lib/InputBuffer.h"
#include "lib/StringRef.h"

#include <fstream>

namespace cmm
{
  
  /// @enum Token Code
  enum class TokenCode : int
  {
    Error = -1,
    None = 0,
    
    Word,
    String,
    Decimal,
    
    Plus, // +
    Minus, // -
    Star, // *
    Slash, // /
    Exclamation, // !
    Ampersand2, // &&
    Pipe2, // ||
    
    Equal, // =
    Equal2, // ==
    Exclamation_Equal, // !=
    LeftAngledBracket, // <
    LeftAngledBracket_Equal, // <=
    RightAngledBracket, // >
    RightAngledBracket_Equal, // >=
    
    LeftParenthesis, // (
    RightParenthesis, // )
    LeftBracket, // [
    RightBracket, // ]
    LeftBrace, // {
    RightBrace, // }
    
    Comma, // ,
    Semicolon, // ,
    
    //
    _count
  };
  
  /// @class Token Builder
  class TokenExtractor
  {
  public:
    explicit TokenExtractor(const char* filename);
    ~TokenExtractor();
    
    bool next();
    
    StringRef tokenData() const;
    TokenCode tokenCode() const;
    
  private:
    std::ifstream m_File;
    InputBuffer m_Buffer;
    
  private:
    void fsmInit();
    bool fsmNextToken();
    bool fsmError();
    
    int m_FsmState;
    const char* m_TokenStart;
    const char* m_TokenEnd;
    const char* m_FsmCurr;
    TokenCode m_TokenCode;
    
  };
  
}
