#include "Context.h"
#include <iostream>

int yyparse(cmm::Context& context);

using namespace cmm;

int main(int argc, char* argv[])
{
  Context context;
  
  if (context.parse())
    std::cerr << argv[0] << ": Parse Passed\n";
  else
    std::cerr << argv[0] << ": Parse Failed\n";
  
  return 0;
}
