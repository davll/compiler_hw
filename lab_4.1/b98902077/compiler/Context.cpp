#include "Context.h"

#include "YYParser.hpp"

namespace cmm
{
  
  Context::Context()
  : m_TokenExtractor("/dev/stdin")
  {
  }
  
  Context::~Context()
  {
  }
  
  // ======================================================================
  
  // ======================================================================
  
  int Context::_lex(YYSTYPE* lvalp)
  {
    m_TokenExtractor.next();
    
    lvalp->null = NULL;
    
    switch (m_TokenExtractor.tokenCode())
    {
      case TokenCode::Error:                    return YX_ERROR;
      case TokenCode::None:                     return 0; // EOF
      case TokenCode::Word:                     return _lex_word(lvalp);
      case TokenCode::String:                   return _lex_string(lvalp);
      case TokenCode::Decimal:                  return _lex_decimal(lvalp);
      case TokenCode::Plus:                     return YX_PLUS;
      case TokenCode::Minus:                    return YX_MINUS;
      case TokenCode::Star:                     return YX_MULTIPLY;
      case TokenCode::Slash:                    return YX_DIVIDE;
      case TokenCode::Exclamation:              return YX_LOGICAL_NOT;
      case TokenCode::Ampersand2:               return YX_LOGICAL_AND;
      case TokenCode::Pipe2:                    return YX_LOGICAL_OR;
      case TokenCode::Equal:                    return YX_ASSIGN;
      case TokenCode::Equal2:                   return YX_EQUAL;
      case TokenCode::Exclamation_Equal:        return YX_NEQUAL;
      case TokenCode::LeftAngledBracket:        return YX_LESS;
      case TokenCode::LeftAngledBracket_Equal:  return YX_LEQUAL;
      case TokenCode::RightAngledBracket:       return YX_GREATER;
      case TokenCode::RightAngledBracket_Equal: return YX_GEQUAL;
      case TokenCode::LeftParenthesis:          return YX_PARENTHESE_OPEN;
      case TokenCode::RightParenthesis:         return YX_PARENTHESE_CLOSE;
      case TokenCode::LeftBracket:              return YX_BRACKET_OPEN;
      case TokenCode::RightBracket:             return YX_BRACKET_CLOSE;
      case TokenCode::LeftBrace:                return YX_BRACE_OPEN;
      case TokenCode::RightBrace:               return YX_BRACE_CLOSE;
      case TokenCode::Comma:                    return YX_COMMA;
      case TokenCode::Semicolon:                return YX_SEMICOLON;
        
      case TokenCode::_count: return YX_ERROR;
    }
    
    return YX_ERROR;
  }
  
  int Context::_lex_word(YYSTYPE* lvalp)
  {
    StringRef data = m_TokenExtractor.tokenData();
    
    if (data.equals("int"))
      return YX_INT;
    else if (data.equals("if"))
      return YX_IF;
    else if (data.equals("else"))
      return YX_ELSE;
    else if (data.equals("while"))
      return YX_WHILE;
    else if (data.equals("break"))
      return YX_BREAK;
    else if (data.equals("continue"))
      return YX_CONTINUE;
    else if (data.equals("scan"))
      return YX_SCAN;
    else if (data.equals("print"))
      return YX_PRINT;
    else if (data.equals("println"))
      return YX_PRINTLN;
    
    // TODO: lvalp
    
    return YX_ID;
  }
  
  int Context::_lex_string(YYSTYPE* lvalp)
  {
    // TODO: lvalp
    
    return YX_STR;
  }
  
  int Context::_lex_decimal(YYSTYPE* lvalp)
  {
    // TODO: lvalp
    
    return YX_NUM;
  }
  
  // ======================================================================
  
}
