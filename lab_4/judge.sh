#!bin/bash

STU="STU=b98902077"

#make clean $STU

make judge $STU PROG=T1A_Hello DATA=1
make judge $STU PROG=T1B_HelloWorld DATA=1
make judge $STU PROG=T1C_HelloWorld DATA=1
make judge $STU PROG=T2A_Echo DATA=1
make judge $STU PROG=T2A_Echo DATA=2
make judge $STU PROG=T2B_Assignment DATA=1
make judge $STU PROG=T2C_Swap DATA=1
make judge $STU PROG=T3A_Arith DATA=1
make judge $STU PROG=T3A_Arith DATA=2
make judge $STU PROG=T3B_Arith DATA=1
make judge $STU PROG=T3C_Arith DATA=1
make judge $STU PROG=T4A_Logic DATA=1
make judge $STU PROG=T4A_Logic DATA=2
make judge $STU PROG=T4A_Logic DATA=3
make judge $STU PROG=T4B_Logic DATA=1
make judge $STU PROG=T4B_Logic DATA=2
make judge $STU PROG=T4B_Logic DATA=3
make judge $STU PROG=T4C_Logic DATA=1
make judge $STU PROG=T4C_Logic DATA=2
make judge $STU PROG=T4C_Logic DATA=3
make judge $STU PROG=T4C_Logic DATA=4
make judge $STU PROG=T4C_Logic DATA=5
make judge $STU PROG=T4C_Logic DATA=6
make judge $STU PROG=T4C_Logic DATA=7
make judge $STU PROG=T4C_Logic DATA=8
make judge $STU PROG=T5A_IfElse DATA=1
make judge $STU PROG=T5A_IfElse DATA=2
make judge $STU PROG=T5A_IfElse DATA=3
make judge $STU PROG=T5A_IfElse DATA=4
make judge $STU PROG=T5A_IfElse DATA=5
make judge $STU PROG=T5A_IfElse DATA=6
make judge $STU PROG=T5A_IfElse DATA=7
make judge $STU PROG=T5B_While DATA=1
make judge $STU PROG=T5C_MulTable DATA=1
make judge $STU PROG=T5D_Break DATA=1
make judge $STU PROG=T5D_Break DATA=2
make judge $STU PROG=T5D_Break DATA=3
make judge $STU PROG=T5E_Break DATA=1
make judge $STU PROG=T6A_Scope DATA=1
make judge $STU PROG=T7A_1dArray DATA=1
make judge $STU PROG=T7B_2dArray DATA=1
make judge $STU PROG=T7C_3dArray DATA=1
make judge $STU PROG=T7D_sumAbs DATA=1
make judge $STU PROG=T7D_sumAbs DATA=2
make judge $STU PROG=T7E_axpy DATA=1
make judge $STU PROG=T7F_mxm DATA=1
make judge $STU PROG=GhostLeg DATA=1 
make judge $STU PROG=GhostLeg DATA=2
make judge $STU PROG=PatternDetection DATA=1
make judge $STU PROG=PatternDetection DATA=2 
make judge $STU PROG=ShrimpLife DATA=1 
make judge $STU PROG=ShrimpLife DATA=2 
make judge $STU PROG=TicTacToe DATA=1 
make judge $STU PROG=TicTacToe DATA=2 
make judge $STU PROG=UVA10608_union DATA=1
make judge $STU PROG=TestString DATA=1
make judge $STU PROG=Kerker DATA=1
make judge $STU PROG=Kerker2 DATA=1
