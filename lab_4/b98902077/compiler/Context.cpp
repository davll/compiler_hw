#include "Context.h"
#include "YYTypes.h"

#include <llvm/Analysis/Verifier.h>
#include <llvm/Support/raw_os_ostream.h>
#include <llvm/Support/IRBuilder.h>
#include <llvm/Support/ToolOutputFile.h>

#include <string>
#include <iostream>
#include <vector>

namespace cmm
{
  
  const bool ALLOCA_SCOPE = false;
  
  Context::Context(MemoryBuffer* buffer)
  : m_TokenExtractor(buffer, m_SourceMgr), m_Builder(m_Context)
  {
    // Prepare Source Code
    m_SourceMgr.AddNewSourceBuffer(buffer, llvm::SMLoc());
    
    // Initialize Module
    
    m_Module = new llvm::Module("Crap", m_Context);
    
    // Init Types
    m_Int32Ty = llvm::Type::getInt32Ty(m_Context);
    m_Int64Ty = llvm::Type::getInt64Ty(m_Context);
    m_Int8PtrTy = llvm::Type::getInt8PtrTy(m_Context);
    m_Int32PtrTy = llvm::Type::getInt32PtrTy(m_Context);
    m_VoidTy = llvm::Type::getVoidTy(m_Context);
    m_SizeTy = m_Int64Ty; // 64-bit machine
    
    // Create Main Function
    llvm::FunctionType* mainT = llvm::FunctionType::get(m_Int32Ty, 
                                                        false);
    
    m_Main = llvm::Function::Create(mainT, 
                                    llvm::GlobalValue::ExternalLinkage,
                                    "main",
                                    m_Module);
    
    m_Main->setCallingConv(llvm::CallingConv::C);
    
    // Get External Functions
    std::vector< llvm::Type* > printfArgs(1, m_Int8PtrTy);
    llvm::FunctionType* printfT = llvm::FunctionType::get(m_Int32Ty, 
                                                          printfArgs, 
                                                          true);
    
    m_Printf = llvm::Function::Create(printfT, 
                                      llvm::GlobalValue::ExternalLinkage,
                                      "printf",
                                      m_Module);
    
    m_Printf->setCallingConv(llvm::CallingConv::C);
    
    m_Scanf = llvm::Function::Create(printfT,
                                     llvm::GlobalValue::ExternalLinkage,
                                     "scanf",
                                     m_Module);
    
    m_Scanf->setCallingConv(llvm::CallingConv::C);
    
    // note: pointer of void is not allowed in LLVM
    std::vector< llvm::Type* > mallocArgs(1, m_SizeTy);
    llvm::FunctionType* mallocT = llvm::FunctionType::get(m_Int8PtrTy, 
                                                          mallocArgs, 
                                                          false);
    
    m_Malloc = llvm::Function::Create(mallocT, 
                                      llvm::GlobalValue::ExternalLinkage,
                                      "malloc",
                                      m_Module);
    
    std::vector< llvm::Type* > freeArgs(1, m_Int8PtrTy);
    llvm::FunctionType* freeT = llvm::FunctionType::get(m_VoidTy, 
                                                        freeArgs, 
                                                        false);
    
    m_Free = llvm::Function::Create(freeT,
                                    llvm::GlobalValue::ExternalLinkage,
                                    "free",
                                    m_Module);
    
    std::vector< llvm::Type* > memsetArgs;
    memsetArgs.push_back(m_Int8PtrTy);
    memsetArgs.push_back(m_Int32Ty);
    memsetArgs.push_back(m_SizeTy);
    llvm::FunctionType* memsetT = llvm::FunctionType::get(m_Int8PtrTy,
                                                          memsetArgs, 
                                                          false);
    
    m_Memset = llvm::Function::Create(memsetT,
                                      llvm::GlobalValue::ExternalLinkage,
                                      "memset",
                                      m_Module);
    
    // Get Intrinsic Functions
    
    m_StackSave = m_Module->getFunction("llvm.stacksave");
    if (!m_StackSave)
    {
      llvm::FunctionType* stackSaveT = llvm::FunctionType::get(m_Int8PtrTy, 
                                                               false);
      m_StackSave = llvm::Function::Create(stackSaveT,
                                           llvm::GlobalValue::ExternalLinkage,
                                           "llvm.stacksave",
                                           m_Module);
    }
    
    m_StackRestore = m_Module->getFunction("llvm.stackrestore");
    if (!m_StackRestore)
    {
      llvm::Function* tmp;
      tmp = llvm::Function::Create(freeT,
                                   llvm::GlobalValue::ExternalLinkage,
                                   "llvm.stackrestore",
                                   m_Module);
      m_StackRestore = tmp;
    }
    
    //
    m_BlockDepth = 0;
    
  }
  
  Context::~Context()
  {
    // Clean Id Table
    for(llvm::StringMap< char* >::iterator 
        i = m_IdNameTable.begin(), e = m_IdNameTable.end(); i != e; ++i)
    {
      char* p = i->getValue();
      std::free(p);
    }
    
    // Delete Module
    delete m_Module;
  }
  
  // ======================================================================
  
  void Context::_codegen_done()
  {
    // Insert Return Instruction
    m_Builder.CreateRet(_get_int_constant("0"));
  }
  
  llvm::Value* Context::_codegen_arith
  (char op, llvm::Value* a, llvm::Value* b, const YYLTYPE& opLoc)
  {
    // according to op, determine instruction
    switch(op)
    {
      case '+': return m_Builder.CreateAdd(a, b, "addTmp");
      case '-': return m_Builder.CreateSub(a, b, "subTmp");
      case '*': return m_Builder.CreateMul(a, b, "mulTmp");
      case '/': return m_Builder.CreateSDiv(a, b, "divTmp");
      case '&': return m_Builder.CreateAnd(a, b, "andTmp");
      case '|': return m_Builder.CreateOr(a, b, "orTmp");
    }
    
    {
      _error(opLoc, "Invalid Binary Arithmetic Expression");
    }
    
    return NULL;
  }
  
  llvm::Value* Context::_codegen_arith
  (char op, llvm::Value* a, const YYLTYPE& opLoc)
  {
    // according to op, determine instruction
    switch(op)
    {
      case '+': return a;
      case '-': return m_Builder.CreateNeg(a, "negTmp");
      case '!': return m_Builder.CreateNot(a, "notTmp");
    }
    
    {
      _error(opLoc, "Invalid Unary Arithmetic Expression");
    }
    
    return NULL;
  }
  
  llvm::Value* Context::_codegen_cmpeq(llvm::Value* a, llvm::Value* b)
  {
    return m_Builder.CreateICmpEQ(a, b);
  }
  
  llvm::Value* Context::_codegen_cmpne(llvm::Value* a, llvm::Value* b)
  {
    return m_Builder.CreateICmpNE(a, b);
  }
  
  llvm::Value* Context::_codegen_cmplt(llvm::Value* a, llvm::Value* b)
  {
    return m_Builder.CreateICmpSLT(a, b);
  }
  
  llvm::Value* Context::_codegen_cmple(llvm::Value* a, llvm::Value* b)
  {
    return m_Builder.CreateICmpSLE(a, b);
  }
  
  llvm::Value* Context::_codegen_cmpgt(llvm::Value* a, llvm::Value* b)
  {
    return m_Builder.CreateICmpSGT(a, b);
  }
  
  llvm::Value* Context::_codegen_cmpge(llvm::Value* a, llvm::Value* b)
  {
    return m_Builder.CreateICmpSGE(a, b);
  }
  
  void Context::_codegen_show(const char* fmt, llvm::Value* val)
  {
    llvm::Value* fmtV = _get_string_constant(fmt);
    m_Builder.CreateCall2(m_Printf, fmtV, val);
  }
  
  void Context::_codegen_show_linefeed()
  {
    llvm::Value* fmtV = _get_string_constant("\n");
    m_Builder.CreateCall(m_Printf, fmtV);
  }
  
  void Context::_codegen_scope_start()
  {
    // Increase block depth
    ++m_BlockDepth;
    
    // New Variable Table
    VarTable* vtable = new VarTable();
    
    // Push to the stack
    m_VarTableList.push_back( vtable );
    
    if (ALLOCA_SCOPE)
    {
      // Save Stack
      llvm::Value* sp = m_Builder.CreateCall(m_StackSave, "___scope");
      (*vtable)["$stack_pointer"] = VariableAttr(sp);
    }
  }
  
  void Context::_codegen_scope_end()
  {
    //
    VarTable* vtable = m_VarTableList.back();
    
    // release resources
    _codegen_scope_interrupt(1);
    
    //
    delete vtable;
    
    // Pop Variable Table
    m_VarTableList.pop_back();
    
    // decrease block depth
    --m_BlockDepth;
  }
  
  void Context::_codegen_scope_interrupt(int n)
  {
    for (std::vector<VarTable*>::reverse_iterator 
         i = m_VarTableList.rbegin(), e = m_VarTableList.rend();
         i != e && n > 0; ++i, --n)
    {
      VarTable* vtable = *i;
      
      // Free memory
      for (VarTable::iterator i2 = vtable->begin(), e2 = vtable->end(); 
           i2 != e2; ++i2)
      {
        if (i2->getValue().need_free) // The type is need to be freed
        {
          llvm::Value* localval = i2->getValue().address;
          
          // Load from stack
          llvm::Value* addr = m_Builder.CreateLoad(localval);
          
          // Cast addr to i8*
          llvm::Value* i8addr = m_Builder.CreateBitCast(addr, m_Int8PtrTy);
          
          // Call Free
          m_Builder.CreateCall(m_Free, i8addr);
        }
      }
      
      if (ALLOCA_SCOPE) 
      {
        // Restore Stack
        llvm::Value* sp = (*vtable)["$stack_pointer"].address;
        m_Builder.CreateCall(m_StackRestore, sp);
      }
    }
  }
  
  llvm::BasicBlock* Context::_codegen_new_block(const char* label)
  {
    llvm::BasicBlock* block = llvm::BasicBlock::Create(m_Context, 
                                                       label, 
                                                       m_Main);
    
    m_Builder.SetInsertPoint(block);
    
    return block;
  }
  
  void Context::_codegen_if(llvm::Value* cond)
  {
    // Create Else block and Push it
    llvm::BasicBlock* elseBlock = llvm::BasicBlock::Create(m_Context, 
                                                           "Else");
    m_LazyBlock.push_back(elseBlock);
    
    // Create Then block
    llvm::BasicBlock* thenBlock = llvm::BasicBlock::Create(m_Context, 
                                                           "Then", 
                                                           m_Main);
    
    // br cond, Then, End
    m_Builder.CreateCondBr(cond, thenBlock, elseBlock); 
    
    // Then:
    m_Builder.SetInsertPoint(thenBlock);
  }
  
  void Context::_codegen_else()
  {
    // Pop Else block
    llvm::BasicBlock* elseBlock = m_LazyBlock.back();
    m_LazyBlock.pop_back();
    
    // Create End block and Push it
    llvm::BasicBlock* endBlock = llvm::BasicBlock::Create(m_Context, 
                                                          "EndIf");
    m_LazyBlock.push_back(endBlock);
    
    // jmp End
    m_Builder.CreateBr(endBlock);
    
    // Else:
    m_Main->getBasicBlockList().push_back( elseBlock ); // Insert it to Main
    m_Builder.SetInsertPoint( elseBlock );
  }
  
  void Context::_codegen_endif()
  {
    // Pop End block
    llvm::BasicBlock* endBlock = m_LazyBlock.back();
    m_LazyBlock.pop_back();
    
    // jmp End
    m_Builder.CreateBr(endBlock);
    
    // End:
    m_Main->getBasicBlockList().push_back( endBlock ); // Insert it to Main
    m_Builder.SetInsertPoint( endBlock );
  }
  
  void Context::_codegen_while()
  {
    // Create Loop block and Push it
    llvm::BasicBlock* loopBlock = llvm::BasicBlock::Create(m_Context, 
                                                           "Loop", 
                                                           m_Main);
    m_LazyBlock.push_back(loopBlock);
    
    // Create LoopEnd block and Push it
    llvm::BasicBlock* endBlock = llvm::BasicBlock::Create(m_Context, 
                                                          "LoopEnd");
    m_LazyBlock.push_back(endBlock);
    
    // Create LoopStart block and Push it
    llvm::BasicBlock* startBlock = llvm::BasicBlock::Create(m_Context, 
                                                            "LoopStart", 
                                                            m_Main);
    m_LazyBlock.push_back(startBlock);
    
    // jmp Loop
    m_Builder.CreateBr(loopBlock);
    
    // Loop:
    m_Builder.SetInsertPoint(loopBlock);
    
    // Push Loop Info
    m_LoopHead.push_back(loopBlock);
    m_LoopEnd.push_back(endBlock);
    m_LoopDepth.push_back(m_BlockDepth);
  }
  
  void Context::_codegen_while_body(llvm::Value* cond)
  {
    // Pop LoopStart block
    llvm::BasicBlock* startBlock = m_LazyBlock.back();
    m_LazyBlock.pop_back();
    
    // Peek LoopEnd block
    llvm::BasicBlock* endBlock = m_LazyBlock.back();
    
    // br cond, LoopStart, LoopEnd
    m_Builder.CreateCondBr(cond, startBlock, endBlock);
    
    // LoopStart:
    m_Builder.SetInsertPoint(startBlock);
  }
  
  void Context::_codegen_endwhile()
  {
    // Pop LoopEnd block
    llvm::BasicBlock* endBlock = m_LazyBlock.back();
    m_LazyBlock.pop_back();
    
    // Pop Loop block
    llvm::BasicBlock* loopBlock = m_LazyBlock.back();
    m_LazyBlock.pop_back();
    
    // jmp Loop
    m_Builder.CreateBr(loopBlock);
    
    // LoopEnd: 
    m_Main->getBasicBlockList().push_back( endBlock ); // Insert it to Main
    m_Builder.SetInsertPoint( endBlock );
    
    // Pop Loop Info
    m_LoopHead.pop_back();
    m_LoopEnd.pop_back();
    m_LoopDepth.pop_back();
  }
  
  void Context::_codegen_new_var(const char* name, const YYLTYPE& loc)
  {
    VarTable* vtable = m_VarTableList.back();
    VarTable::iterator it = vtable->find(name);
    
    if (it == vtable->end()) // If the variable is new to the scope
    {
      if (m_DeclDims.size() == 0) // This is Local Variable
      {
        // allocate from stack
        llvm::Value* localvar = m_Builder.CreateAlloca(m_Int32Ty, 0, name);
        
        // clear zero
        m_Builder.CreateStore(_get_int_constant("0"), localvar);
        
        // add to VarTable
        vtable->GetOrCreateValue(name, VariableAttr(localvar));
      }
      else // This is Array Variable
      {
        // Compute size
        llvm::Value* siz = _get_int_constant("4");
        for (std::vector<llvm::Value*>::iterator i = m_DeclDims.begin(), 
             e = m_DeclDims.end(); i != e; ++i)
        {
          llvm::Value* d = *i;
          siz = m_Builder.CreateMul(siz, d);
        }
        
        // Cast to size_t
        llvm::Value* param = m_Builder.CreateSExt(siz, m_SizeTy);
        
        // Call malloc
        llvm::Value* i8addr = m_Builder.CreateCall(m_Malloc, param);
        
        // Cast i8* to i32*
        llvm::Value* addr = m_Builder.CreateBitCast(i8addr, m_Int32PtrTy);
        
        // clear zero
        m_Builder.CreateCall3(m_Memset, i8addr, _get_int_constant("0"), param);
        
        // allocate localvar
        llvm::Value* localvar = m_Builder.CreateAlloca(m_Int32PtrTy, 0, name);
        
        // store address
        m_Builder.CreateStore(addr, localvar);
        
        // add to VarTable
        vtable->GetOrCreateValue(name, VariableAttr(localvar, m_DeclDims));
      }
    }
    else // duplicate name is not allowed
    {
      _error(loc, Twine("Duplicate name \"") + name + "\" in declaration");
    }
  }
  
  llvm::Value* Context::_codegen_find_var
  (const char* name, const DimensionVector* dims, const YYLTYPE& loc)
  {
    // Find a variable
    for (std::vector<VarTable*>::reverse_iterator 
         i = m_VarTableList.rbegin(), 
         e = m_VarTableList.rend();
         i != e; ++i)
    {
      VarTable::iterator pVar = (*i)->find(name);
      if (pVar != (*i)->end()) // found
      {
        VariableAttr& var = pVar->getValue();
        
        if (var.dims.size() == 0) // Local Variable
        {
          if (dims != NULL)
          {
            _error(loc, "local variable should not have dimension");
          }
          
          return var.address;
        }
        else // Array
        {
          if (dims == NULL || dims->size() != var.dims.size())
          {
            _error(loc, "the dimension of the array does not match");
          }
          
          llvm::Value* localval = var.address;
          
          // Load Address
          llvm::Value* addr = m_Builder.CreateLoad(localval);
          
          // Compute Array Element Offset
          std::vector<llvm::Value*>::const_iterator indexI = dims->begin();
          std::vector<llvm::Value*>::const_iterator indexE = dims->end();
          std::vector<llvm::Value*>::iterator sizeI = var.dims.begin();
          
          llvm::Value* offset = _get_int_constant("0");
          
          for (; indexI != indexE; ++indexI, ++sizeI)
          {
            offset = m_Builder.CreateMul(offset, *sizeI);
            offset = m_Builder.CreateAdd(offset, *indexI);
          }
          
          return m_Builder.CreateInBoundsGEP(addr, offset);
        }
      }
    }
    
    // Not Found
    _error(loc, Twine("Cannot find variable with the name \"") + name +"\"");
    return NULL;
  }
  
  llvm::Value* Context::_codegen_load(llvm::Value* ptr)
  {
    return m_Builder.CreateLoad(ptr);
  }
  
  void Context::_codegen_store(llvm::Value* ptr, llvm::Value* expr)
  {
    m_Builder.CreateStore(expr, ptr);
  }
  
  void Context::_codegen_scanf(llvm::Value* ptr)
  {
    llvm::Value* fmtV = _get_string_constant("%d");
    m_Builder.CreateCall2(m_Scanf, fmtV, ptr);
  }
  
  void Context::_codegen_decl_nodim()
  {
    m_DeclDims.clear();
  }
  
  void Context::_codegen_decl_dims(const std::vector<llvm::Value*>& dims)
  {
    m_DeclDims = dims;
  }
  
  void Context::_codegen_break(const YYLTYPE& loc)
  {
    unsigned long depth = m_BlockDepth;
    
    if (m_LoopDepth.empty())
    {
      _error(loc, "The break statement must be used in loop");
    }
    
    // Clean up resources
    _codegen_scope_interrupt( depth - m_LoopDepth.back() );
    
    // jmp LoopEnd
    m_Builder.CreateBr( m_LoopEnd.back() );
    
    // Next:
    llvm::BasicBlock* block = llvm::BasicBlock::Create(m_Context, 
                                                       "Next", 
                                                       m_Main);
    m_Builder.SetInsertPoint(block);
  }
  
  void Context::_codegen_continue(const YYLTYPE& loc)
  {
    unsigned long depth = m_BlockDepth;
    
    if (m_LoopDepth.empty())
    {
      _error(loc, "The continue statement must be used in loop");
    }
    
    // Clean up resources
    _codegen_scope_interrupt( depth - m_LoopDepth.back() );
    
    // jmp Loop
    m_Builder.CreateBr( m_LoopHead.back() );
    
    // Next:
    llvm::BasicBlock* block = llvm::BasicBlock::Create(m_Context, 
                                                       "Next", 
                                                       m_Main);
    m_Builder.SetInsertPoint(block);
  }
  
  // ======================================================================
  
  int Context::_lex(YYSTYPE* lvalp, YYLTYPE* llocp)
  {
    // Next Token!
    m_TokenExtractor.next();
    
    // Initialize Default Value
    lvalp->null = NULL;
    
    // Get Location
    llocp->startPtr = m_TokenExtractor.tokenLocStart().getPointer();
    llocp->endPtr = m_TokenExtractor.tokenLocEnd().getPointer();
    
    //
    //std::cerr << "Lex: " << m_TokenExtractor.tokenData().str() << " : ";
    //std::cerr << "(" << (int)m_TokenExtractor.tokenCode() << ") ";
    //std::cerr << (void*) llocp->startPtr << ", ";
    //std::cerr << (void*) llocp->endPtr << "\n";
    
    // Determine Token Code
    switch (m_TokenExtractor.tokenCode())
    {
      case TokenCode::Error:                    return YX_ERROR;
      case TokenCode::None:                     return 0; // EOF
      case TokenCode::Word:                     return _lex_word(lvalp);
      case TokenCode::String:                   return _lex_string(lvalp);
      case TokenCode::Decimal:                  return _lex_decimal(lvalp);
      case TokenCode::Plus:                     return YX_PLUS;
      case TokenCode::Minus:                    return YX_MINUS;
      case TokenCode::Star:                     return YX_MULTIPLY;
      case TokenCode::Slash:                    return YX_DIVIDE;
      case TokenCode::Exclamation:              return YX_LOGICAL_NOT;
      case TokenCode::Ampersand2:               return YX_LOGICAL_AND;
      case TokenCode::Pipe2:                    return YX_LOGICAL_OR;
      case TokenCode::Equal:                    return YX_ASSIGN;
      case TokenCode::Equal2:                   return YX_EQUAL;
      case TokenCode::Exclamation_Equal:        return YX_NEQUAL;
      case TokenCode::LeftAngledBracket:        return YX_LESS;
      case TokenCode::LeftAngledBracket_Equal:  return YX_LEQUAL;
      case TokenCode::RightAngledBracket:       return YX_GREATER;
      case TokenCode::RightAngledBracket_Equal: return YX_GEQUAL;
      case TokenCode::LeftParenthesis:          return YX_PARENTHESE_OPEN;
      case TokenCode::RightParenthesis:         return YX_PARENTHESE_CLOSE;
      case TokenCode::LeftBracket:              return YX_BRACKET_OPEN;
      case TokenCode::RightBracket:             return YX_BRACKET_CLOSE;
      case TokenCode::LeftBrace:                return YX_BRACE_OPEN;
      case TokenCode::RightBrace:               return YX_BRACE_CLOSE;
      case TokenCode::Comma:                    return YX_COMMA;
      case TokenCode::Semicolon:                return YX_SEMICOLON;
        
      case TokenCode::_count: return YX_ERROR;
    }
    
    // Error
    return YX_ERROR;
  }
  
  int Context::_lex_word(YYSTYPE* lvalp)
  {
    StringRef data = m_TokenExtractor.tokenData();
    
    // Check Keywords
    if (data.equals("int"))
      return YX_INT;
    else if (data.equals("if"))
      return YX_IF;
    else if (data.equals("else"))
      return YX_ELSE;
    else if (data.equals("while"))
      return YX_WHILE;
    else if (data.equals("break"))
      return YX_BREAK;
    else if (data.equals("continue"))
      return YX_CONTINUE;
    else if (data.equals("scan"))
      return YX_SCAN;
    else if (data.equals("print"))
      return YX_PRINT;
    else if (data.equals("println"))
      return YX_PRINTLN;
    
    // OK, It is Identifier, not keyword
    
    StringRef s = m_TokenExtractor.tokenData();
    
    // Lookup
    llvm::StringMap<char*>::iterator it = m_IdNameTable.find(s);
    
    if (it == m_IdNameTable.end())
    {
      // Add Entry
      char* d = (char*) std::malloc(s.size()+1);
      std::memcpy(d, s.data(), s.size());
      d[s.size()] = '\0';
      
      m_IdNameTable[s] = d;
      
      it = m_IdNameTable.find(s);
    }
    
    // Get Name
    lvalp->name = it->getValue();
    
    // Return Token Code
    return YX_ID;
  }
  
  int Context::_lex_string(YYSTYPE* lvalp)
  {
    StringRef _s = m_TokenExtractor.tokenData();
    
    // Get Value Ptr
    lvalp->value = _get_string_constant(_s, true);
    
    // Return Token Code
    return YX_STR;
  }
  
  int Context::_lex_decimal(YYSTYPE* lvalp)
  {
    StringRef d = m_TokenExtractor.tokenData();
    
    lvalp->value = _get_int_constant(d);
    
    // Return Token Code
    return YX_NUM;
  }
  
  // ======================================================================
  
  llvm::Constant* Context::_get_int_constant(StringRef s)
  {
    // Abstract Precision Integer
    llvm::APInt intVal(32, s, 10);
    
    // Return Value Ptr from LLVM Context
    return llvm::ConstantInt::get(m_Context, intVal);
  }
  
  llvm::Value* Context::_get_string_constant(StringRef s, bool escap)
  {
    llvm::StringMap< llvm::Value* >::iterator it = m_StringTable.find(s);
    
    if (it == m_StringTable.end()) // If the entry is not found
    {
      if (escap) // Eat Escape Sequences
      {
        // Create Temporary String
        char* d = (char*) std::malloc(s.size()+1);
        
        // Convert Escape Sequences to real codes
        char* p = d;
        for (const char *i = s.begin()+1, *e = s.end(); i != e; ++i, ++p)
        {
          if (*i == '\\')
            *p = *(++i); // Eat Escape Sequence
          else
            *p = *i;
        }
        *(--p) = '\0';
        
        //std::cerr << d << '\n';
        
        // Query from LLVM Context
        llvm::Value* val = m_Builder.CreateGlobalStringPtr(d);
        //llvm::ConstantDataArray::getString(m_Context, d, true);
        
        // Add to Table
        m_StringTable[s] = val;
        
        // Delete Temporary String
        std::free(d);
      }
      else
      {
        // Query from LLVM Context
        llvm::Value* val = m_Builder.CreateGlobalStringPtr(s);
        
        // Add to Table
        m_StringTable[s] = val;
      }
      
      it = m_StringTable.find(s);
    }
    
    return it->getValue();
  }
  
  bool Context::printIR(const char* outputFile)
  {
    if (llvm::verifyModule(*m_Module, llvm::PrintMessageAction))
    {
      return false;
    }
    
    //
    std::string err;
    
    //
    llvm::tool_output_file out(outputFile, err);
    
    if (!err.empty())
    {
      std::cerr << err << std::endl;
      return false;
    }
    
    // Write IR Code
    m_Module->print(out.os(), NULL);
    
    // Keep file
    out.keep();
    
    return true;
  }
  
  void Context::_error(const YYLTYPE& llocp, const Twine& msg)
  {
    SourceLocation startLoc = SourceLocation::getFromPointer(llocp.startPtr);
    SourceLocation endLoc = SourceLocation::getFromPointer(llocp.endPtr);
    llvm::SMRange range(startLoc, endLoc) ;
    
    m_SourceMgr.PrintMessage(endLoc, llvm::SourceMgr::DK_Error, msg, 
                             llvm::ArrayRef<llvm::SMRange>( range ));
    
    std::exit(1);
  }
  
}
