%%{
  machine cmm_TokenExtractor;
  
  alnum_u = alnum | '_';
  alpha_u = alpha | '_';
  
  long_comment = '/*' any* :>> '*/';
  
  string = '"' ( (^cntrl-[\\"]) | ('\\' [\\"]) )* :>> '"';
  
  word = alpha_u alnum_u*;
  
  dec = [0]|([1-9][0-9]*);
  hex = '0x' [0-9A-Fa-f]+;
  
  main := |*
    
    long_comment;
    space;
    
    #
    word { tc = TokenCode::Word; fbreak; };
    
    #
    string { tc = TokenCode::String; fbreak; };
    
    #
    dec { tc = TokenCode::Decimal; fbreak; };
    
    #
    '+' { tc = TokenCode::Plus; fbreak; };
    '-' { tc = TokenCode::Minus; fbreak; };
    '*' { tc = TokenCode::Star; fbreak; };
    '/' { tc = TokenCode::Slash; fbreak; };
    
    #
    '!' { tc = TokenCode::Exclamation; fbreak; };
    '&&' { tc = TokenCode::Ampersand2; fbreak; };
    '||' { tc = TokenCode::Pipe2; fbreak; };
    
    #
    '=' { tc = TokenCode::Equal; fbreak; };
    
    #
    '==' { tc = TokenCode::Equal2; fbreak; };
    '!=' { tc = TokenCode::Exclamation_Equal; fbreak; };
    '<' { tc = TokenCode::LeftAngledBracket; fbreak; };
    '<=' { tc = TokenCode::LeftAngledBracket_Equal; fbreak; };
    '>' { tc = TokenCode::RightAngledBracket; fbreak; };
    '>=' { tc = TokenCode::RightAngledBracket_Equal; fbreak; };
    
    #
    '(' { tc = TokenCode::LeftParenthesis; fbreak; };
    ')' { tc = TokenCode::RightParenthesis; fbreak; };
    '[' { tc = TokenCode::LeftBracket; fbreak; };
    ']' { tc = TokenCode::RightBracket; fbreak; };
    '{' { tc = TokenCode::LeftBrace; fbreak; };
    '}' { tc = TokenCode::RightBrace; fbreak; };
    
    #
    ',' { tc = TokenCode::Comma; fbreak; };
    ';' { tc = TokenCode::Semicolon; fbreak; };
    
  *|;
  
}%%

#include "TokenExtractor.h"
#include <cassert>

namespace cmm
{
  namespace
  {
    %% write data;
  }
  
  void TokenExtractor::fsmInit()
  {
    // Init Start State
    m_FsmState = %%{ write start; }%% ;
    
    // Init Token Information
    m_TokenStart = NULL;
    m_TokenEnd = NULL;
    m_TokenCode = TokenCode::None;
    
    // Init Read Pointer
    m_FsmCurr = m_Buffer->getBufferStart();
  }
  
  bool TokenExtractor::fsmNextToken()
  {
    int cs = m_FsmState;
    const char *ts = NULL, *te = NULL;
    
    const char *p = m_FsmCurr, *pe = m_Buffer->getBufferEnd();
    const char *eof = pe;
    TokenCode::Type tc = TokenCode::None;
    
    // Run Finite State Machine
    %% write exec;
    
    m_FsmState = cs;
    m_TokenCode = tc;
    
    if (fsmError() || tc == TokenCode::Error)
    {
      m_FsmCurr = p;
      m_TokenStart = ( ts == NULL ? p : ts );
      m_TokenEnd = p;
      m_TokenCode = TokenCode::Error;
      return false;
    }
    else if (tc != TokenCode::None)// OK
    {
      m_FsmCurr = te;
      m_TokenStart = ts;
      m_TokenEnd = te;
    }
    else
    {
      m_FsmCurr = pe;
      m_TokenStart = pe;
      m_TokenEnd = pe;
    }
    
    return tc != TokenCode::None;
  }
  
  bool TokenExtractor::fsmError()
  {
    return (m_FsmState == %%{ write error; }%%);
  }
  
  // ===============================================================================
  
  TokenExtractor::TokenExtractor(MemoryBuffer* buffer, llvm::SourceMgr& mgr)
  : m_Buffer(buffer), m_SrcMgr(mgr), m_TokenStart(NULL), m_TokenEnd(NULL)
  {
    assert((buffer != NULL) && "MemoryBuffer should be valid");
    
    fsmInit();
  }
  
  TokenExtractor::~TokenExtractor()
  {
  }
  
  bool TokenExtractor::next()
  {
    if (fsmError())
      return false;
    
    return fsmNextToken();
  }
  
  StringRef TokenExtractor::tokenData() const
  {
    if (m_TokenCode == TokenCode::None)
      return StringRef(NULL, 0);
    
    return StringRef(m_TokenStart, m_TokenEnd - m_TokenStart);
  }
  
  TokenCode::Type TokenExtractor::tokenCode() const
  {
    return m_TokenCode;
  }
  
  SourceLocation TokenExtractor::tokenLocStart() const
  {
    return SourceLocation::getFromPointer(m_TokenStart);
  }
  
  SourceLocation TokenExtractor::tokenLocEnd() const
  {
    return SourceLocation::getFromPointer(m_TokenEnd - 1);
  }
  
}
