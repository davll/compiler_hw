#include "lib/InputBuffer.h"

#include <iostream>

using namespace cmm;

int main()
{
  InputBuffer inputbuf(std::cin);
  
  std::cout << "size = " << inputbuf.size() << "\n";
  
  while(!inputbuf.eof())
  {
    inputbuf.update();
  }
  
  std::cout << "size = " << inputbuf.size() << "\n";
  
  for(InputBuffer::iterator i = inputbuf.begin(), e = inputbuf.end();
      i != e; ++i)
  {
    std::cout << *i;
  }
  
  return 0;
}
