#pragma once

#include <llvm/ADT/StringRef.h>
#include <llvm/Support/MemoryBuffer.h>
#include <llvm/Support/SMLoc.h>
#include <llvm/Support/SourceMgr.h>
#include <llvm/ADT/Twine.h>
#include <llvm/ADT/ArrayRef.h>

#include <fstream>

namespace cmm
{
  using llvm::MemoryBuffer;
  using llvm::StringRef;
  using llvm::Twine;
  typedef llvm::SMLoc SourceLocation;
  
  /// @enum Token Code
  struct TokenCode
  {
    enum Type
    {
      Error = -1,
      None = 0,
      
      Word,
      String,
      Decimal,
      
      Plus, // +
      Minus, // -
      Star, // *
      Slash, // /
      Exclamation, // !
      Ampersand2, // &&
      Pipe2, // ||
      
      Equal, // =
      Equal2, // ==
      Exclamation_Equal, // !=
      LeftAngledBracket, // <
      LeftAngledBracket_Equal, // <=
      RightAngledBracket, // >
      RightAngledBracket_Equal, // >=
      
      LeftParenthesis, // (
      RightParenthesis, // )
      LeftBracket, // [
      RightBracket, // ]
      LeftBrace, // {
      RightBrace, // }
      
      Comma, // ,
      Semicolon, // ,
      
      //
      _count
    };
  };
  
  /// @class Token Builder
  class TokenExtractor
  {
  public:
    explicit TokenExtractor(MemoryBuffer* buffer, llvm::SourceMgr& mgr);
    ~TokenExtractor();
    
    bool next();
    
    StringRef tokenData() const;
    TokenCode::Type tokenCode() const;
    SourceLocation tokenLocStart() const;
    SourceLocation tokenLocEnd() const;
    
  private:
    MemoryBuffer* m_Buffer;
    llvm::SourceMgr& m_SrcMgr;
    
  private:
    void fsmInit();
    bool fsmNextToken();
    bool fsmError();
    
    int m_FsmState;
    const char* m_TokenStart;
    const char* m_TokenEnd;
    const char* m_FsmCurr;
    TokenCode::Type m_TokenCode;
    
  };
  
}
