#include "Context.h"
#include <iostream>

#include <llvm/Support/CommandLine.h>
#include <llvm/Support/Signals.h>
#include <llvm/Support/PrettyStackTrace.h>
#include <llvm/ADT/OwningPtr.h>
#include <llvm/Support/system_error.h>
#include <llvm/Support/ToolOutputFile.h>

using namespace llvm;
using namespace cmm;

namespace
{
  
  cl::opt<std::string> 
  inputFileName (cl::Positional, 
                 //cl::Required, 
                 //cl::OneOrMore, 
                 cl::desc("<input file>"),
                 cl::init("-")
                 );
  
  
  cl::opt<std::string>
  outputFileName("o", 
                 cl::desc("Specify output filename"), 
                 cl::value_desc("filename"),
                 cl::init("-")
                 );
  
  /*
  cl::opt<bool> emitAsm("S", cl::desc("Emit LLVM IR Assembly"));
  
  enum OptLv {
    O0, O1, O2, O3
  };
  
  cl::opt<OptLv> optmizationLv(cl::desc("Choose Optimizaiton level:"),
                               cl::values(clEnumVal(O0, "None"),
                                          clEnumVal(O1, "Trivial"),
                                          clEnumVal(O2, "Default"),
                                          clEnumVal(O3, "Heavy"),
                                          clEnumValEnd));
  */
}

int main(int argc, char* argv[])
{
  // Setup LLVM Bug-Trace System
  sys::PrintStackTraceOnErrorSignal();
  PrettyStackTraceProgram _stackTrace(argc, argv);
  
  // Parse Arguments
  cl::ParseCommandLineOptions( argc, argv, "C-- compiler front-end\n" );
  // inputFileName.c_str()
  
  // 
  OwningPtr<MemoryBuffer> buffer;
  llvm::error_code err;
  
  // Memory Buffer Creation
  err = MemoryBuffer::getFileOrSTDIN(inputFileName.c_str(), buffer);
  
  if (err) // Failed to open file and create buffer
  {
    // Print Diagnostic Message
    SMDiagnostic(inputFileName.c_str(), llvm::SourceMgr::DK_Error,
                 "Cannot open input file: " + err.message())
    .print(argv[0], llvm::errs());
    
    std::exit(1); // Exit
  }
  
  // Init Context and take over memroy buffer
  Context context(buffer.take());
  
  // Parse!
  if (context.parse())
  {
    std::cerr << argv[0] << ": Parse Passed\n";
    
    context.printIR( outputFileName.c_str() );
  }
  else
    std::cerr << argv[0] << ": Parse Failed\n";
  
  return 0;
}
