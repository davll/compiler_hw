#pragma once

// Location Tracking

struct YYLTYPE
{
  const char* startPtr;
  const char* endPtr;
  // [startPtr, endPtr], not [startPtr, endPtr)
};

#define YYLTYPE YYLTYPE

#define YYLLOC_DEFAULT(curr, rhs, n) \
do \
{ \
  if( n > 0 ) \
  { \
    (curr).startPtr = YYRHSLOC((rhs), 1).startPtr; \
    (curr).endPtr = YYRHSLOC((rhs), (n)).endPtr; \
  } \
  else \
  { \
    (curr) = YYRHSLOC((rhs), 0); \
  } \
} \
while(0)

// Token Code

#include "YYParser.hpp"
