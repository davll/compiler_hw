%{

#include "Context.h"
#include "YYTypes.h"

#include <llvm/Support/SMLoc.h>

#include <iostream>

// Token Information

union YYSTYPE;

static int yylex(YYSTYPE *lvalp, YYLTYPE *llocp, cmm::Context& context);

static int yyparse(cmm::Context& context);

static void yyerror(YYLTYPE *llocp, cmm::Context& context, const char* msg);

%}

%require "2.3"

%output="YYParser.cpp"
%defines

%locations
%pure-parser
/*%define api.pure*/

// yylex args
%lex-param   { cmm::Context& context }

// yyparse args
%parse-param { cmm::Context& context }

// Terminal + Non-Terminal Data Types
%union {
  void* null;
  const char* name;
  llvm::Value* value;
  cmm::DimensionVector* pDims;
  std::vector<const char*>* pNames;
}

// Tokens
%token <null> YX_ERROR
%token <name> YX_ID
%token <value> YX_STR
%token <value> YX_NUM
%token <null> YX_INT                 "int"
%token <null> YX_IF                  "if"
%token <null> YX_ELSE                "else"
%token <null> YX_WHILE               "while"
%token <null> YX_BREAK               "break"
%token <null> YX_CONTINUE            "continue"
%token <null> YX_SCAN                "scan"
%token <null> YX_PRINT               "print"
%token <null> YX_PRINTLN             "println"

// Operator Tokens
%token <null> YX_PLUS                 "+"
%token <null> YX_MINUS                "-"
%token <null> YX_MULTIPLY             "*"
%token <null> YX_DIVIDE               "/"
%token <null> YX_LOGICAL_NOT          "!"
%token <null> YX_LOGICAL_AND          "&&"
%token <null> YX_LOGICAL_OR           "||"
%token <null> YX_ASSIGN               "="
%token <null> YX_EQUAL                "=="
%token <null> YX_NEQUAL               "!="
%token <null> YX_LESS                 "<"
%token <null> YX_LEQUAL               "<="
%token <null> YX_GREATER              ">"
%token <null> YX_GEQUAL               ">="
%token <null> YX_PARENTHESE_OPEN      "("
%token <null> YX_PARENTHESE_CLOSE     ")"
%token <null> YX_BRACKET_OPEN         "["
%token <null> YX_BRACKET_CLOSE        "]"
%token <null> YX_BRACE_OPEN           "{"
%token <null> YX_BRACE_CLOSE          "}"
%token <null> YX_COMMA                ","
%token <null> YX_SEMICOLON            ";"

// Operator Precedence
%left "||"
%left "&&"
%left "!"
%nonassoc "==" "!=" "<" "<=" ">" ">="
%left "+" "-"
%left "*" "/"
%right YX_UNARY

// Define Start Symbol
%start program

// Define Types of Non-Terminals
%type <value> arithExpr logicExpr
%type <value> var
%type <pDims> dims

%%

// Start Symbol
program:   { context._codegen_new_block("Start"); } 
         blockStmt 
           { context._codegen_done(); }
       ;

// Block Sematics
blockStmt: "{" { context._codegen_scope_start(); } 
           declSeq 
           stmtSeq 
           "}" { context._codegen_scope_end();}
         ;

// Variable Declaration Sequence
declSeq: declSeq decl
       | /* NULL */
       ;

// Statement Sequence
stmtSeq: stmtSeq stmt
       | /* NULL */
       ;

// Variable Declaration
decl: "int" declLocal declNames ";"
    | "int" declArray declNames ";"
    ;

declLocal: { context._codegen_decl_nodim(); } ;
declArray: dims { context._codegen_decl_dims(*($1)); delete $1; } ;

// Identifier Sequence
declNames: declNames "," YX_ID { context._codegen_new_var($3, @3); }
         | YX_ID { context._codegen_new_var($1, @1); }
         ;

// Dimension Sequence
dims: dims "[" arithExpr "]" { $1->push_back($3); $$ = $1; }
    | "[" arithExpr "]" { $$ = new cmm::DimensionVector(1, $2); }
    ;

// Statement: Variable Assignment
stmt: var "=" arithExpr ";" { context._codegen_store($1, $3); } ;

// Statement: IF branch
stmt: ifClause blockStmt endifClause
    | ifClause blockStmt elseClause blockStmt endifClause
    ;

// IF-ELSE-ENDIF clauses
ifClause: "if" "(" logicExpr ")" { context._codegen_if($3); } ;
elseClause: "else" { context._codegen_else(); } ;
endifClause: { context._codegen_endif(); } ;

// Notes:
// http://compilers.iecc.com/comparch/article/98-01-079
// http://ubuntuforums.org/showthread.php?t=787626
// http://docs.freebsd.org/info/bison/bison.info.Shift_Reduce.html
// http://uw714doc.sco.com/en/SDK_tools/_Ambiguity_and_Conflicts.html


// Statement: WHILE loop
stmt: "while" { context._codegen_while(); }
      "(" logicExpr ")" { context._codegen_while_body($4); }
      blockStmt { context._codegen_endwhile(); }
    ;

// Statement: break the while loop
stmt: "break" ";" { context._codegen_break(@1); } ;

// Statement: continue the while loop
stmt: "continue" ";" { context._codegen_continue(@1); } ;

// Statement: read from input
stmt: "scan" "(" readVarList ")" ";";

// Statement: print something
stmt: "print" "(" printableList ")" ";";

// Statement: print something and line-feed
stmt: "println" "(" printableList ")" ";" 
      { context._codegen_show_linefeed(); }
    ;

// Statement: Block
stmt: blockStmt;

// Variable Sequence for scanf
readVarList: readVarList "," var { context._codegen_scanf($3); }
           | var { context._codegen_scanf($1); }
           ;

// Printable Object Sequence
printableList: printableList "," printable 
             | printable
             ;

// Printable Object
printable: YX_STR { context._codegen_show("%s", $1); }
         | arithExpr { context._codegen_show("%d", $1); }
         ;

// Variable Access
var: YX_ID { $$ = context._codegen_find_var($1, NULL, @1); }
   | YX_ID dims { $$ = context._codegen_find_var($1, $2, @1); delete $2; }
   ;

// Arithmetic Expression (Binary)
arithExpr: arithExpr "+" arithExpr {$$ = context._codegen_arith('+',$1,$3,@2);}
         | arithExpr "-" arithExpr {$$ = context._codegen_arith('-',$1,$3,@2);}
         | arithExpr "*" arithExpr {$$ = context._codegen_arith('*',$1,$3,@2);}
         | arithExpr "/" arithExpr {$$ = context._codegen_arith('/',$1,$3,@2);}
         ;

// Arithmetic Expression (Unary)
arithExpr: "+" arithExpr %prec YX_UNARY {$$=context._codegen_arith('+',$2,@1);}
         | "-" arithExpr %prec YX_UNARY {$$=context._codegen_arith('-',$2,@1);}
         | var    { $$ = context._codegen_load($1); }
         | YX_NUM { $$ = $1; }
         | "(" arithExpr ")" { $$ = $2; }
         ;

// Logical Expression (Logic Operation)
logicExpr: logicExpr "||" logicExpr {$$=context._codegen_arith('|',$1,$3,@2);}
         | logicExpr "&&" logicExpr {$$=context._codegen_arith('&',$1,$3,@2);}
         | "!" logicExpr            {$$ = context._codegen_arith('!',$2,@1);}
         | "[" logicExpr "]" { $$ = $2; }
         ;

// Logical Expression (Condition)
logicExpr: arithExpr "==" arithExpr {$$ = context._codegen_cmpeq($1, $3);}
         | arithExpr "!=" arithExpr {$$ = context._codegen_cmpne($1, $3);}
         | arithExpr "<" arithExpr  {$$ = context._codegen_cmplt($1, $3);}
         | arithExpr "<=" arithExpr {$$ = context._codegen_cmple($1, $3);}
         | arithExpr ">" arithExpr  {$$ = context._codegen_cmpgt($1, $3);}
         | arithExpr ">=" arithExpr {$$ = context._codegen_cmpge($1, $3);}
         ;

%%

//
//
//
static int yylex(YYSTYPE *lvalp, YYLTYPE *llocp, cmm::Context& context)
{
  int result = context._lex(lvalp, llocp);
  
  return result;
}

//
//
//
static void yyerror(YYLTYPE *llocp, cmm::Context& context, const char* msg)
{
  context._error(*llocp, msg);
}

//
//
//
namespace cmm
{
  
  bool Context::parse()
  {
    return yyparse(*this) == 0;
  }
  
}

