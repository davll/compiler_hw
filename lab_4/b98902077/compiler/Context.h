#pragma once

#include "TokenExtractor.h"

#include <llvm/DerivedTypes.h>
#include <llvm/LLVMContext.h>
#include <llvm/Module.h>
#include <llvm/Value.h>
#include <llvm/ADT/StringMap.h>
#include <llvm/Instructions.h>
#include <llvm/Support/IRBuilder.h>
#include <llvm/Support/SourceMgr.h>
#include <llvm/Support/SMLoc.h>

#include <vector>
#include <map>
#include <stack>

struct YYLTYPE;
union YYSTYPE;

namespace cmm
{
  
  typedef std::vector<llvm::Value*> DimensionVector;
  
  class Context
  {
  public:
    Context(MemoryBuffer* buffer);
    ~Context();
    
    bool parse();
    
    bool printIR(const char* outputFile);
    
    llvm::Module* getModule(){ return m_Module; }
    
  public: // for YYParser.y
    // Error
    void _error(const YYLTYPE& llocp, const Twine& msg);
    
    // Lex
    int _lex(YYSTYPE* lvalp, YYLTYPE* llocp);
    
    // Code Generation
    void _codegen_done();
    
    llvm::Value* _codegen_arith
    (char op, llvm::Value* a, llvm::Value* b, const YYLTYPE& opLoc);
    
    llvm::Value* _codegen_arith
    (char op, llvm::Value* a, const YYLTYPE& opLoc);
    
    llvm::Value* _codegen_cmpeq(llvm::Value* a, llvm::Value* b);
    llvm::Value* _codegen_cmpne(llvm::Value* a, llvm::Value* b);
    llvm::Value* _codegen_cmplt(llvm::Value* a, llvm::Value* b);
    llvm::Value* _codegen_cmple(llvm::Value* a, llvm::Value* b);
    llvm::Value* _codegen_cmpgt(llvm::Value* a, llvm::Value* b);
    llvm::Value* _codegen_cmpge(llvm::Value* a, llvm::Value* b);
    
    void _codegen_show(const char* fmt, llvm::Value* val);
    void _codegen_show_linefeed();
    
    /// @note: block and scope are different!
    
    void _codegen_scope_start();
    void _codegen_scope_end();
    void _codegen_scope_interrupt(int n);
    
    llvm::BasicBlock* _codegen_new_block(const char* label);
    
    void _codegen_if(llvm::Value* cond);
    void _codegen_else();
    void _codegen_endif();
    
    void _codegen_while();
    void _codegen_while_body(llvm::Value* cond);
    void _codegen_endwhile();
    
    void _codegen_new_var(const char* name, const YYLTYPE& loc);
    
    llvm::Value* _codegen_find_var
    (const char* name, const DimensionVector* dims, const YYLTYPE& loc);
    
    llvm::Value* _codegen_load(llvm::Value* ptr);
    void _codegen_store(llvm::Value* ptr, llvm::Value* expr);
    
    void _codegen_scanf(llvm::Value* ptr);
    
    void _codegen_decl_nodim();
    void _codegen_decl_dims(const std::vector<llvm::Value*>& dims);
    
    void _codegen_break(const YYLTYPE& loc);
    void _codegen_continue(const YYLTYPE& loc);
    
  private:
    int _lex_word(YYSTYPE* lvalp);
    int _lex_string(YYSTYPE* lvalp);
    int _lex_decimal(YYSTYPE* lvalp);
    
    llvm::Constant* _get_int_constant(StringRef s);
    llvm::Value* _get_string_constant(StringRef s, bool escap = false);
    
  private: // 
    Context(const Context&);
    void operator=(const Context&);
    
  private:
    TokenExtractor m_TokenExtractor;
    
    // Source Manager
    llvm::SourceMgr m_SourceMgr;
    
    // LLVM Context
    llvm::LLVMContext m_Context;
    
    // Root Module
    llvm::Module* m_Module;
    
    // Main Function
    llvm::Function* m_Main;
    
    // String Literal Table
    llvm::StringMap< llvm::Value* > m_StringTable;
    
    // Name Table
    llvm::StringMap< char* > m_IdNameTable;
    
    // Functions
    llvm::Function* m_Printf;
    llvm::Function* m_Scanf;
    llvm::Function* m_Malloc;
    llvm::Function* m_Free;
    llvm::Function* m_StackSave;
    llvm::Function* m_StackRestore;
    llvm::Function* m_Memset;
    
    // Types
    llvm::Type* m_Int32Ty;
    llvm::Type* m_Int64Ty;
    llvm::PointerType* m_Int8PtrTy;
    llvm::PointerType* m_Int32PtrTy;
    llvm::Type* m_SizeTy;
    llvm::Type* m_VoidTy;
    
    // Working Block
    llvm::IRBuilder<> m_Builder;
    std::vector< llvm::BasicBlock* > m_LazyBlock;
    DimensionVector m_DeclDims;
    unsigned long m_BlockDepth;
    
    // Loop Info
    std::vector< llvm::BasicBlock* > m_LoopHead;
    std::vector< llvm::BasicBlock* > m_LoopEnd;
    std::vector< unsigned long >     m_LoopDepth;
    
    
    // Variable Table
    struct VariableAttr
    {
      llvm::Value* address;
      bool need_free;
      std::vector< llvm::Value* > dims;
      
      VariableAttr() : address(0), need_free(false) {}
      
      VariableAttr(const VariableAttr& o)
      : address(o.address), need_free(o.need_free), dims(o.dims) {}
      
      VariableAttr& operator=(const VariableAttr& o)
      {
        if (this != &o)
          address = o.address, need_free = o.need_free, dims = o.dims;
        return *this;
      }
      
      explicit VariableAttr(llvm::Value* ptr)
      : address(ptr), need_free(false)
      {
      }
      
      explicit VariableAttr
      (llvm::Value* ptr, const std::vector<llvm::Value*>& d)
      : address(ptr), need_free(true), dims(d)
      {
      }
      
    };
    
    typedef llvm::StringMap< VariableAttr > VarTable;
    std::vector< VarTable* > m_VarTableList;
    
  };
  
}
