//
// C-- Compiler
//
// AUTHOR: David Lin
// STUDENT ID: b98902077
// Selected IR: LLVM (Low-Level Virtual Machine)
//

1. The Compiler is based on LLVM framework. It can generate LLVM IR code.

2. Dependencies: LLVM 3.1 libraries

3. Its result (IR code) can be applied with LLVM's Optimization, Native Executable Generation
   
   opt -S -O2 -o <Output IR File> <Input IR File> # optimization with -O2 option
   llvm-as -o <Output BitCode File> <Input IR File> # translate asm file to binary file
   llvm-ld -o <Native Executable File> <Input BitCode File> # Generate Target Executable
   
4. Besides, it has beautiful diagnostic message system.
   Once there's some error, the compiler will point out the place and the reason.

