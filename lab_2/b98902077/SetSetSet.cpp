#include "GLexer.h"
#include "Production.h"

#include <iostream>
#include <list>
#include <queue>
#include <set>
#include <algorithm>

#include <stdlib.h>
#include <string.h>

// cat ../grammarE4.g | ./SetSetSet

// ========================================================================

static void parseProductions(SymbolTable& symtable, std::vector<Production>& productions)
{
  for( CToken tok = CToken::get(); tok.is_valid(); tok = CToken::get() )
  {
    //std::cout << "[" << tok.code() << ":" << tok.data() << "]\n";
    
    if(tok.code() != GTOKEN_WORD)
    {
      std::cerr << "Error: WORD is expected. (LHS)\n";
      exit(1);
    }
    
    // Add LHS to SymbolTable
    int symbol_lhs = symtable.getSymbol(tok);
    
    tok = CToken::get();
    if(tok.is_null() || tok.code() != GTOKEN_COLON)
    {
      std::cerr << "Error: COLON is expected. (LHS:)\n";
      exit(2);
    }
    
    // RHS list
    std::list<int> symbol_rhs;
    
    while(tok.code() != GTOKEN_SEMICOLON)
    {
      tok = CToken::get();
      
      if(tok.is_null())
      {
        std::cerr << "Error: SEMICOLON is expected before $.\n";
        exit(3);
      }
      
      switch(tok.code())
      {
        case GTOKEN_SEMICOLON:
        case GTOKEN_VBAR:
          // Emit production
          {
            int uid = productions.size();
            productions.push_back(Production(symbol_lhs, symbol_rhs, uid));
          }
          // reset RHS list
          symbol_rhs.clear();
          break;
        case GTOKEN_WORD:
        case GTOKEN_STRING:
          // Add RHS symbol to the list
          symbol_rhs.push_back( symtable.getSymbol(tok) );
          break;
        default:
          std::cerr << "Error: Illegal token.\n";
          exit(4);
      }
    }
  }
}

// ========================================================================

static void printSet(const SymbolTable& symtable, const std::set<int>& pset);
static void printStringCsv(std::ostream& ost, const std::string& str);

int main()
{
  SymbolTable symtable;
  std::vector<Production> productions;
  
  // Parse
  parseProductions(symtable, productions);
  
  // Symbol is Terminal or Non-Terminal
  std::vector<bool> sym_terminal(symtable.count(), true);
  {
    for(const Production& p : productions)
      sym_terminal[ p.lhs() ] = false;
  }
  
  // 1-Level back tracking
  std::vector< std::set<int> > sym_backtrace(symtable.count());
  {
    for(const Production& p : productions)
      for(int r : p.rhs())
        sym_backtrace[r].insert(p.uid());
  }
  
  // 1-Level forward
  std::vector< std::list<int> > sym_forward(symtable.count());
  {
    for(const Production& p : productions)
      sym_forward[p.lhs()].push_back(p.uid());
  }
  
  // Symbol can derive lambda or not
  std::vector<bool> sym_lambda(symtable.count(), false);
  sym_lambda[SymbolTable::SYMBOL_LAMBDA] = true;
  {
    std::vector< std::pair< int, std::list<int> > > reduced(productions.size());
    
    // initialize reducible production table
    for(const Production& p : productions)
    {
      reduced[p.uid()].first = p.lhs();
      reduced[p.uid()].second.assign(p.rhs().begin(), p.rhs().end());
    }
    
    //
    std::vector<bool> pending(productions.size());
    std::queue<int> work;
    
    // push lambda production to the work queue
    for(const Production& p : productions)
      if(p.lambda())
        work.push(p.uid()), pending[p.uid()] = true;
    
    // Bottom-Up BFS
    while(!work.empty())
    {
      // extract reducible production
      int k = work.front(); work.pop();
      auto& rhs = reduced[k].second;
      int lhs = reduced[k].first;
      
      // reduce production
      for(auto it = rhs.begin(); it != rhs.end();)
        if(sym_lambda[*it])
          it = rhs.erase(it);
        else
          break;
      
      // if the RHS is empty now, the LHS derives lambda
      if(rhs.size() == 0 && !sym_lambda[lhs])
      {
        sym_lambda[lhs] = true;
        
        // go up
        for(int pid : sym_backtrace[lhs])
          if(!pending[pid])
            pending[pid] = true, work.push(pid);
      }
      
      pending[k] = false;
    }
  }
  
  // Compute First-Set
  std::vector< std::set<int> > sym_first(symtable.count());
  {
    // first-sets of terminals
    for(int s = SymbolTable::SYMBOL_OTHER; s < symtable.count(); s++)
      if(sym_terminal[s])
        sym_first[s].insert(s);
    
    // 
    std::vector<bool> pending(productions.size());
    std::queue<int> work;
    
    // push pre-terminal production to the work queue
    for(int s = SymbolTable::SYMBOL_OTHER; s < symtable.count(); s++)
      if(sym_terminal[s])
        for(int up : sym_backtrace[s])
          if(!pending[up])
            work.push(up), pending[up] = true;
    
    // Bottom-up BFS
    while(!work.empty())
    {
      auto k = work.front(); work.pop();
      const Production& p = productions[k];
      
      auto oldsize = sym_first[p.lhs()].size();
      
      for(int r : p.rhs())
      {
        if(r != p.lhs())
        {
          const auto& fs = sym_first[r];
          sym_first[p.lhs()].insert(fs.begin(), fs.end());
        }
        if(!sym_lambda[r])
          break;
      }
      
      if(sym_first[p.lhs()].size() != oldsize)
      {
        for(int up : sym_backtrace[p.lhs()])
          if(!pending[up])
            pending[up] = true, work.push(up);
      }
      
      pending[k] = false;
    }
    
    // add lambda to the first-sets of non-terminals that can derive lambda
    for(int s = SymbolTable::SYMBOL_OTHER; s < symtable.count(); s++)
      if(sym_lambda[s])
        sym_first[s].insert(SymbolTable::SYMBOL_LAMBDA);
  }
  
  // Start Symbol
  int sym_start = productions.front().lhs();
  
  // Compute Follow-Set
  std::vector< std::set<int> > sym_follow(symtable.count());
  {
    // add EOF to the follow-set of the start symbol
    sym_follow[sym_start].insert(SymbolTable::SYMBOL_EOF);
    
    // scan and fill
    for(const Production& p : productions)
    {
      for(auto it = p.rhs().begin(); it != p.rhs().end(); ++it)
      {
        int a = *it;
        
        auto it2 = it;
        for(++it2; it2 != p.rhs().end(); ++it2)
        {
          int b = *it2;
          sym_follow[a].insert( sym_first[b].begin(), sym_first[b].end() );
          if(!sym_lambda[b])
            break;
        }
      }
    }
    
    // scan and fill
    {
      std::vector<bool> pending(productions.size(), false);
      std::queue<int> work;
      
      for(const auto& p : productions)
        work.push(p.uid()), pending[p.uid()] = true;
      
      while(!work.empty())
      {
        int k = work.front(); work.pop();
        const auto& p = productions[k];
        
        for(auto it = p.rhs().begin(); it != p.rhs().end(); ++it)
        {
          int a = *it;
          auto it2 = it;
          for(++it2; it2 != p.rhs().end(); ++it2)
            if(!sym_lambda[*it2])
              break;
          if(it2 == p.rhs().end())
          {
            const auto& sf = sym_follow[p.lhs()];
            auto oldsize = sym_follow[a].size();
            sym_follow[a].insert( sf.begin(), sf.end() );
            if( oldsize != sym_follow[a].size() )
              for(int fw : sym_forward[a])
                if(!pending[fw])
                  work.push(fw), pending[fw] = true;
          }
        }
        
        pending[k] = false;
      }
    }
    
    // remove lambda from follow-sets
    for(int s = SymbolTable::SYMBOL_OTHER; s < symtable.count(); s++)
      sym_follow[s].erase(SymbolTable::SYMBOL_LAMBDA);
  }
  
  // Compute Predict-Set
  std::vector< std::set<int> > prod_predict(productions.size());
  {
    for(const Production& p : productions)
    {
      std::set<int>& ps = prod_predict[p.uid()];
      
      bool will_derive_lambda = true;
      
      for(int r : p.rhs())
      {
        const auto& fs = sym_first[r];
        ps.insert( fs.begin(), fs.end() );
        if(!sym_lambda[r])
        {
          will_derive_lambda = false;
          break;
        }
      }
      
      ps.erase(SymbolTable::SYMBOL_LAMBDA);
      
      if(will_derive_lambda)
      {
        const auto& fs = sym_follow[p.lhs()];
        ps.insert( fs.begin(), fs.end() );
      }
    }
  }
  
  // Print CSV
  {
    std::cout << "\" LHS \",\" RHS \",\" FIRST \",\" FOLLOW \",\" PREDICT \"\n";
    int s = 0;
    for(const Production& p : productions)
    {
      if(s != p.lhs())
      {
        s = p.lhs();
        
        std::cout << "\" " << symtable.data(s) << " \",";
        std::cout << "\"";
        for(int r : p.rhs())
        {
          std::cout << " ";
          printStringCsv(std::cout, symtable.data(r));
        }
        std::cout << " \",";
        
        printSet(symtable, sym_first[s]);
        std::cout << ",";
        
        printSet(symtable, sym_follow[s]);
        std::cout << ",";
        
        printSet(symtable, prod_predict[p.uid()]);
        std::cout << "\n";
      }
      else
      {
        std::cout << ",\"";
        for(int r : p.rhs())
        {
          std::cout << " ";
          printStringCsv(std::cout, symtable.data(r));
        }
        std::cout << " \",,,";
        
        printSet(symtable, prod_predict[p.uid()]);
        std::cout << "\n";
      }
    }
  }
  
  // Show
  if(0){
    int s = 0;
    for(const Production& p : productions)
    {
      if(s != p.lhs())
      {
        s = p.lhs();
        std::cout << "  SYM[" << symtable.data(s) << "]: \n";
        std::cout << "    terminal = " << sym_terminal[s] << "\n";
        std::cout << "    empty = " << sym_lambda[s] << "\n";
        std::cout << "    first-set = { ";
        for(int k : sym_first[s])
          std::cout << symtable.data(k) << "[" << k <<"]" << " ";
        std::cout << "}\n";
        std::cout << "    follow-set = { ";
        for(int k : sym_follow[s])
          std::cout << symtable.data(k) << "[" << k <<"]" << " ";
        std::cout << "}\n";
      }
      
      std::cout << symtable.data(s) << " ->";
      for(int r : p.rhs()) std::cout << " " << symtable.data(r);
      std::cout << " : predict-set = { ";
      for(int k : prod_predict[p.uid()])
        std::cout << symtable.data(k) << "[" << k << "]" << " ";
      std::cout << "}\n";
      
    }
  }
  
  return 0;
}

// ========================================================================

class SymbolComparator
{
public:
  SymbolComparator(const SymbolTable& st):symtable(st){}
  
  bool operator()(int a, int b)const
  {
    if(symtable.code(a) != symtable.code(b))
      return symtable.code(a) < symtable.code(b);
    return symtable.data(a) < symtable.data(b);
  }
  
private:
  const SymbolTable& symtable;
  
};

static void printStringCsv(std::ostream& ost, const std::string& str)
{
  for(auto it = str.begin(); it != str.end(); ++it)
  {
    char ch = *it;
    if(ch == '\\')
    {
      ch = *++it;
      switch(ch)
      {
        case '\"':
          ost << "\"\"";
          break;
        case '\\':
          ost << "\\";
          break;
      }
    }
    else
      ost << ch;
  }
}

static void printSet(const SymbolTable& symtable, const std::set<int>& pset)
{
  std::vector<int> v(pset.begin(), pset.end());
  
  std::sort(v.begin(), v.end(), SymbolComparator(symtable));
  
  std::cout << "\"";
  for(int i : v)
  {
    std::cout << " ";
    printStringCsv(std::cout, symtable.data(i));
  }
  std::cout << " \"";
}
