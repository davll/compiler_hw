#pragma once

#include <stddef.h>
#include <istream>

//
//
//
typedef const struct GToken* GTokenRef;

enum GTokenCode
{
  GTOKEN_NONE = 0,
  GTOKEN_WORD,
  GTOKEN_STRING,
  GTOKEN_SEMICOLON,
  GTOKEN_COLON,
  GTOKEN_VBAR
};

int GTokenCode(GTokenRef token);
const char* GTokenData(GTokenRef token);
void GTokenRetain(GTokenRef token);
void GTokenRelease(GTokenRef token);
int GTokenCompare(GTokenRef t1, GTokenRef t2);

//
//
//
class GLexer
{
private:
  void initRagel();
  GTokenRef execRagel();
  
public:
  GLexer(std::istream& stream);
  ~GLexer();
  
  GTokenRef getToken();
  
private:
  void updateInput();
  void shrinkInput(char* te);
  
private:
  std::istream& m_Stream;
  
  char* m_InputBuffer;
  char* m_InputBufferEnd;
  
  char* m_DataPtr;
  char* m_DataPtrEnd;
  char* m_DataEof;
  
  int m_RagelCs;
  int m_RagelAct;
  char* m_RagelTs;
  char* m_RagelTe;
};

//
// ****************************************************************************
//

#include <iostream>

//
//
//
class CToken
{
public:
  CToken(const CToken& token)
    : m_TokenObj(token.m_TokenObj)
  {
    if(m_TokenObj)
      GTokenRetain(m_TokenObj);
  }
  
  ~CToken()
  {
    if(m_TokenObj)
      GTokenRelease(m_TokenObj);
  }
  
  CToken()
    : m_TokenObj(NULL)
  {
  }
  
  const CToken& operator=(const CToken& token)
  {
    if(m_TokenObj)
      GTokenRelease(m_TokenObj);
    m_TokenObj = token.m_TokenObj;
    if(m_TokenObj)
      GTokenRetain(m_TokenObj);
    return *this;
  }
  
  static CToken get()
  {
    static GLexer lex(std::cin);
    GTokenRef token = lex.getToken();
    return CToken(token);
  }
  
  static CToken null()
  {
    return CToken(NULL);
  }
  
  bool is_null()const
  {
    return m_TokenObj == NULL;
  }
  
  bool is_valid()const
  {
    return m_TokenObj != NULL;
  }
  
  const char* data()const
  {
    return GTokenData(m_TokenObj);
  }
  
  int code()const
  {
    return GTokenCode(m_TokenObj);
  }
  
  bool operator==(const CToken& token)const
  {
    return GTokenCompare(m_TokenObj, token.m_TokenObj) == 0;
  }
  
  bool operator<(const CToken& token)const
  {
    return GTokenCompare(m_TokenObj, token.m_TokenObj) < 0;
  }
  
private:
  CToken(GTokenRef obj)
    : m_TokenObj(obj)
  {
  }
  
private:
  GTokenRef m_TokenObj;
  
};
