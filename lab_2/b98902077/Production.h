#pragma once

#include "GLexer.h"

#include <string.h>

#include <string>
#include <list>
#include <vector>
#include <map>
#include <iterator>

class SymbolTable
{
public:
  enum{
    SYMBOL_EOF = 0,
    SYMBOL_LAMBDA,
    SYMBOL_OTHER
  };
  
public:
  SymbolTable()
    : m_Count(SYMBOL_OTHER)
  {
    m_Data.push_back(std::string("(eof)")); // EOF
    m_Code.push_back(GTOKEN_NONE);
    
    m_Data.push_back(std::string("(null)")); // LAMBDA
    m_Code.push_back(GTOKEN_NONE);
  }
  
  ~SymbolTable()
  {
  }
  
  int getSymbol(const CToken& tok)
  {
    std::map<CToken, int>::iterator iter = m_Lookup.find(tok);
    
    if(iter != m_Lookup.end())
      return iter->second;
    
    switch(tok.code())
    {
      case GTOKEN_WORD:
        m_Lookup.insert(std::make_pair(CToken(tok), m_Count++));
        m_Data.push_back(std::string(tok.data()));
        m_Code.push_back(tok.code());
        break;
      case GTOKEN_STRING:
        m_Lookup.insert(std::make_pair(CToken(tok), m_Count++));
        m_Data.push_back(std::string(tok.data(), 1, strlen(tok.data())-2));
        m_Code.push_back(tok.code());
        break;
      default:
        std::cerr << "Error: the token must be WORD or STRING for Symbol.\n";
        return -1;
    }
    
    return m_Count-1;
  }
  
  const std::string data(int symbol)const
  {
    return m_Data[symbol];
  }
  
  int code(int symbol)const
  {
    return m_Code[symbol];
  }
  
  int count()const
  {
    return m_Count;
  }
  
private:
  int m_Count;
  std::map<CToken, int> m_Lookup;
  std::vector<std::string> m_Data;
  std::vector<int> m_Code;
  
};

class Production
{
public:
  template<typename T>
  class tmp_array
  {
  public:
    tmp_array(const T* p, int siz):m_Size(siz), m_Ptr(p) {  }
    tmp_array(const tmp_array& obj):m_Size(obj.m_Size), m_Ptr(obj.m_Ptr) {  }
    
    int size()const{ return m_Size; }
    int operator[](int i)const{ return m_Ptr[i]; }
    
    class const_iterator : public std::iterator<std::input_iterator_tag, T>
    {
      const int *p;
    public:
      const_iterator(const int *_p):p(_p){}
      const_iterator(const const_iterator& it):p(it.p){}
      const_iterator& operator++(){ ++p; return *this; }
      const_iterator operator++(int){ const_iterator t(*this); operator++(); return t; }
      bool operator==(const const_iterator& rhs)const{ return p==rhs.p; }
      bool operator!=(const const_iterator& rhs)const{ return p!=rhs.p; }
      int operator*()const{ return *p; }
    };
    
    const_iterator begin()const{ return const_iterator(m_Ptr); }
    const_iterator end()const{ return const_iterator(m_Ptr+m_Size); }
    
  private:
    int m_Size;
    const T* m_Ptr;
  };
  
  int lhs()const{ return m_List[1]; }
  tmp_array<int> rhs()const{ return tmp_array<int>(m_List+2, m_NumOfRhs); }
  
  bool lambda()const
  {
    return (m_NumOfRhs == 1 && m_List[2] == SymbolTable::SYMBOL_LAMBDA);
  }
  
  int uid()const{ return m_Uid; }
  
public:
  Production(const Production& prod)
    : m_Uid(prod.m_Uid), m_List(prod.m_List), m_NumOfRhs(prod.m_NumOfRhs)
  {
    m_List[0]++;
  }
  
  ~Production()
  {
    m_List[0]--;
    if(m_List[0] == 0)
      delete[] m_List;
  }
  
  Production(int lhs, const std::list<int>& rhs, int uid)
    : m_Uid(uid)
  {
    if(rhs.size() == 0) // Lambda
      m_NumOfRhs = 1;
    else
      m_NumOfRhs = rhs.size();
    
    m_List = new int[m_NumOfRhs + 2];
    
    m_List[0] = 1;
    m_List[1] = lhs;
    
    if(rhs.size() == 0)
      m_List[2] = SymbolTable::SYMBOL_LAMBDA;
    else
    {
      std::list<int>::const_iterator it = rhs.begin(), itend = rhs.end();
      for(int i = 0; it != itend; ++it, ++i)
        m_List[2+i] = *it;
    }
  }
  
private:
  int m_Uid;
  int* m_List;
  int m_NumOfRhs;
  // size = N (RHS) + 1 (LHS) + 1 (refcount)
};

// ========================================================================



